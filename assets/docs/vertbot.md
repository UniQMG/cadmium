** Vertbot integration **

As of Vertbot 2.2.0, Cadmium includes an experimental `output vertbot` node.
This requires Vertbot and Cadmium to be in the same server, and plays wav
output from Cadmium in vertbot

Additionally as part of this patch, audioupload has been replaced with several
type-sensitive audioupload extensions, audioupload-wav, audioupload-mp3, and
audioupload-flac. Additionally, `input youtube` now outputs flac audio.

Audio is kept around for 24 hours, or 1 hour of inactivity, whichever comes first.
Attempting to play expired audio results in vertbot instantly playing the next song.

**Known issues**
Vertbot does not like playing nightcored audio with a nonstandard sample rate.
A new pipeline element, `transform resample`, was added which resamples audio
without changing speed or pitch. This restores the sample rate to 44100, so it
can play on vertbot or other samplerate-sensitive audio players.

Example pipeline:
`input youtube dQw4w9WgXcQ | transform audio-to-wav | transform nightcore 150% | transform resample | output vertbot`

Once pipeline processing starts, vertbot will notify the channel with an embed:
```
Awaiting Cadmium song
Song will be added to queue on pipeline complete
```

After processing is done, vertbot will reply again with either a success message
or a failure message. Vertbot has **2 types of failure messages**

The first type is a general error, and indicates that an internal error occured.
Changing your pipeline will not fix this.
```
Error: Error playing Cadmium track
This appears to be an internal issue, and changing your command will likely not help. Try again later`
```

The second type of error means lavalink cannot play the pipeline. The most likely
reason is that you changed samplerate (see above) without `transform resample`
```
Error: Unable to play Cadmium track
Please check your Cadmium command, or Cadmium's vertbot.md manual
```

**Advanced Pipeline v1.0 Help**

**v1.0 pipelines have *BEEN REMOVED* and replaced with v2.0 pipelines**

A pipeline is a series of data-processing nodes which are linked together
to get data from point A to point B. Pipeline objects are nodes in the
pipeline which can connect to other nodes.

There are 3 main categories of nodes, Inputs, Transforms, and Outputs.
Generally inputs only send data to other nodes, outputs only accept data
from other nodes, and transforms accept and send data to other nodes.
To use a pipeline object from a category, specify the category name
and then the input name. Pipe characters ('|') connect nodes together,
sending data from the node on the left of the pipe to the one on the right.

For example,

  `input echo Hello, world! | output reply`

creates an input node called 'echo' which writes text to the next node,
which is an output node called 'reply' which sends that text as a reply.

Nodes have strict typing, so for example you cannot connect an 'echo'
(outputs text) node to an audioupload (accepts audio:mp3 or audio:wav) node.
However, an 'audio' input can accept any subtype of audio. For example,
'audio' accepts 'audio:mp3', but 'audio:mp3' does not accept 'audio:wav'.

Full list of nodes:
%pagebreak
**v1.0 pipelines have *BEEN REMOVED* and replaced with v2.0 pipelines**
```
INPUTS (input x y)
  echo <text>         [] -> text
  	sends the given text into the pipeline

  youtube <link>  [text] -> audio:wav
  	streams a youtube video as audio based on a
  	given video ID or url which can be specified
  	directly or piped in as text

  url <link>          [] -> audio:[aac,ogg,wav,mp3,mp4]
    streams an audio file based on a given url

  ytsearch <text>     [] -> text
  	searches for a youtube video using the given
  	search text, then outputs it's ID. Most useful
  	when piped into the youtube input

```
%pagebreak
**v1.0 pipelines have *BEEN REMOVED* and replaced with v2.0 pipelines**
```
TRANSFORMS (transform x y)
  audio-to-pcm       [audio] -> pcm:s16le
  	converts any audio to pcm

  pcm-to-wav     [pcm:s16le] -> audio:wav
    converts pcm to wav.

  audio-to-mp3       [audio] -> audio:mp3
  	converts any audio to mp3

  audio-to-wav       [audio] -> audio:wav
  	converts any audio to wav

  volume <vol>%  [pcm:s16le] -> pcm:s16le
    adjusts audio of incoming pcm data. This node can
    be adjusted while the pipeline is running using
    the 'volume' command.

  nightcore <speed>% [audio] -> audio:wav
  	adjusts the speed and pitch of the incoming
  	audio. 100% is regular speed and pitch.

  speed <speed>%     [audio] -> audio:wav
  	adjusts the speed of the incoming audio without
  	modifying pitch. 100% is regular speed.

  equalizer <dB>x10  [audio] -> audio:wav
  	accepts 10 numbers which correspond to dB changes
  	at 10 different frequency bands. For example, a
  	mild bass boost would be `equalizer 2 5 8 3 1 0 0 0 0 0`

  compressor <threshold> <ratio> <attack> <release> [audio] -> audio:wav
    applies a compressor filter to audio with the given
    threshold (default 0.125), ratio (default 2), attack
    (in ms, default 20) and release (in ms, default 250)

```
%pagebreak
**v1.0 pipelines have *BEEN REMOVED* and replaced with v2.0 pipelines**
```
OUTPUTS (output x y)
  stdout      [text]
  	debug/owner only: prints text to process stdout
    DEPRECATED: owner only

  speaker     [audio:pcm]
  	debug/owner only: plays raw PCM directly to speakers
    DEPRECATED: owner only
    REMOVED 11/13/18

  file        [*]
    experimental/owner only: Writes data to a file to be re-used later
    DEPRECATED: owner only

  ffplay      [pcm:s16le]
    debug/owner only: plays music using ffplay directly to speakers
    DEPRECATED: owner only

  reply       [text]
  	prints text as a reply in discord

  voicechat   [audio:wav]
  	plays audio in a voicechat. The bot must be joined to
  	a voice chat by using `caddy vc` first.

  audioupload [audio:wav, audio:mp3]
  	uploads audio as a playable sound file. Throws an error
  	if the incoming data exceeds the upload limit.
```

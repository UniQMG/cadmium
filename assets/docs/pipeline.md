**Advanced Pipeline v2.0 Help**

**In beta**

A pipeline is a series of data-processing nodes which are linked together
to transform data from A to B. Pipeline objects are nodes in the pipeline
which are connected to other nodes.

There are three categories of nodes: Inputs, Transforms, and Outputs.
Inputs generally produce data, transforms change that data into another
format, and outputs consume data.

Pipelines are constructed using bash-like syntax. First you put the
category, then the node name, then arguments for the node. To connect
nodes together, use a pipe ('|') character.

For example,
  `input echo Hello, world! | output reply`

This example creates an 'echo' input node which writes some text to
the 'reply' output node. The reply node consumes the text, sending it
as a discord reply.

Nodes are strictly typed and can accept only specific types of data.
Transform nodes are used both for converting formats and changing the
actual logical data being sent.

See page 3 for input nodes
See page 4 for transform nodes
See page 5 for output nodes
%pagebreak
**Details on limitations and how the pipeline system works**
When you request a pipeline, that request is sent to a node-processing
process. These processes can be hosted on multiple machines to achieve
scalability, and all communication is encrypted and anonymous.

While a node is processing, a streaming http request is kept open to
send back chunks of data. The bot process then reassembles these
chunks and sends them wherever requested (voice chat, mp3 upload, etc.)

Each processing server has a limited number of concurrent pipelines it
will accept, to ensure availability and performance, and thus the bot's
overall number of simultaneous pipelines is limited.

To attempt to fairly share resources, each discord user is limited to
2 simultaneous pipelines and a limit of 500MiB of data per pipeline.
Data usage is calculated as the sum of all nodes' output data, including
nodes which are connected to other nodes. Data usage grows linearly with
the number of nodes, though uncompressed data formats such as .wav use
significantly more data than more cpu-intensive ones such as .mp3
%pagebreak
```
INPUTS (input x y)
  echo <text>         [] -> text
  	sends the given text into the pipeline

  youtube <link>  [text] -> audio:wav
  	streams a youtube video as audio based on a
  	given video ID or url which can be specified
  	directly or piped in as text

  url <link>          [] -> audio:[aac,ogg,wav,mp3,mp4]
    streams an audio file based on a given url

  ytsearch <text>     [] -> text
  	searches for a youtube video using the given
  	search text, then outputs it's ID. Most useful
  	when piped into input youtube.
```
%pagebreak
```
TRANSFORMS (transform x y)
  audio-to-mp3       [audio] -> audio:mp3
  	converts any audio to mp3

  audio-to-wav       [audio] -> audio:wav
  	converts any audio to wav

  nightcore <speed>% [audio] -> audio:wav
  	adjusts the speed and pitch of the incoming
  	audio. 100% is regular speed and pitch.
    See vertbot.md for considerations when using vertbot.

  speed <speed>%     [audio] -> audio:wav
  	adjusts the speed of the incoming audio without
  	modifying pitch. 100% is regular speed.

  equalizer <dB>x10  [audio] -> audio:wav
  	accepts 10 numbers which correspond to dB changes
  	at 10 different frequency bands. For example, a
  	mild bass boost would be `equalizer 2 5 8 3 1 0 0 0 0 0`

  compressor <threshold> <ratio> <attack> <release> [audio] -> audio:wav
    applies a compressor filter to audio with the given
    threshold (default 0.125), ratio (default 2), attack
    (in ms, default 20) and release (in ms, default 250)

  resample [audio:wav] -> audio:wav
    Resamples wav audio to 44100Hz without changing it.
    Primarily used to cancel side-effects of nightcore
    without un-nightcoring the song.
```
%pagebreak
```
OUTPUTS (output x y)
  reply       [text]
  	prints text as a reply in discord

  voicechat   [audio:wav]
  	Plays audio in voice chat.

  audioupload-mp3 [audio:mp3]
  	Uploads mp3 audio as a playable sound file.

  audioupload-wav [audio:wav]
  	Uploads wav audio as a playable sound file.

  audioupload-m4a [audio:m4a]
  	Uploads m4a audio as a playable sound file.

  audioupload-flac [audio:flac]
  	Uploads flac audio as a playable sound file.

  vertbot [audio:wav]
  	Adds pipeline output to vertbot.
    See vertbot.md for more info.
```

**QNHABIAA** (Questions nobody has asked but I'm answering anyway)

**Q:** Why?
**A:** Because.

**Q:** Why is nothing realtime/why is the bot so slow/bad playlist.
**A:** Cadmium is meant as an audio/data processing bot, not a music
   playback bot. You're encouraged to process audio ahead of time and
   use another bot to play the audio, such as vertbot.

**Q:** Support server?
**A:** Listed in the info command.

© UniQMG 2018-${new Date().getFullYear()}

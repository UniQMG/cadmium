#!/bin/bash
# This horribly convoluted script gathers metadata about a node
# and then generates a certificate for it
# Also generates the vertbot server certificate if the 3rd argument is 'vertbot'
./genroot.sh

domain=$1
if [ -z "$domain" ]; then
  printf "Enter domain name: "
  read domain
fi

hash=$(git rev-parse HEAD | head -c 7)

if [ "$3" = "vertbot" ]; then
  echo "Building for vertbot on domain $domain"
else
  echo Building for domain $domain and commit hash $hash
fi

if [ ! -e ssl/clients/$domain/ ] || [ "$3" = "vertbot" ]; then
  printf "Generating new certificate\n"
  cfssl serve -ca-key ./ssl/ca-key.pem -ca ./ssl/ca.pem -config ./ssl/config-ca.json > /dev/null 2>&1 &
  sleep 1
  sed -e "s/\${HOSTNAME}/$domain/" template/csr_client.json > ssl/csr_client.json
  cfssl gencert -config ssl/config-client.json ssl/csr_client.json | cfssljson -bare ssl/db

  if [ "$3" = "vertbot" ]; then
    mkdir ssl/vertbot
    mv ssl/db-key.pem ssl/vertbot/
    mv ssl/db.pem ssl/vertbot/
    mv ssl/db.csr ssl/vertbot/

    if [ -f ssl/vertbot/secret ]; then
      echo "Reusing previous vertbot password"
    else
      echo "Generating new vertbot password"
      hexdump -n 16 -e '4/4 "%08X"' /dev/random > ssl/vertbot/secret
    fi
  else
    mkdir -p ssl/clients/$domain
    mv ssl/db-key.pem ssl/clients/$domain/
    mv ssl/db.pem ssl/clients/$domain/
    mv ssl/db.csr ssl/clients/$domain/

    pipelineLimit=$2
    if [ -z "$pipelineLimit" ]; then
      while [[ ! "$pipelineLimit" =~ ^[0-9]+ ]]; do
          printf "Pipeline limit:"
          read pipelineLimit
      done
    fi

    # kill cfssl
    kill $! > /dev/null 2>&1

    echo $domain > ssl/clients/$domain/domain
    echo $pipelineLimit > ssl/clients/$domain/pipelineLimit
    if [ "$domain" = "localhost" ]; then
      echo "WARNING: Using default, insecure password for building localhost"
      echo "password" > ssl/clients/$domain/secret
    else
      hexdump -n 32 -e '4/4 "%08X"' /dev/random > ssl/clients/$domain/secret
    fi
  fi
else
  printf "Certificate already exists\n"
fi
printf "Done\n"

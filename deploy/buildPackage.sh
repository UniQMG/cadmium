#!/bin/bash
hash=$(git rev-parse HEAD | head -c 7)
echo Building for commit hash $hash

domain=$1
if [ -z "$domain" ]; then
  printf "Enter domain name: "
  read domain
fi
echo $domain

if [ ! -e "ssl/clients/$domain/" ]; then
  echo "Domain \"$domain\" not found, has the client been created?"
  exit 1
fi

cp template/serverconfig.yaml ../config/serverconfig.yaml
cp ssl/clients/$domain/db-key.pem ../config/caddynode-db-key.pem
cp ssl/clients/$domain/db.pem ../config/caddynode-db.pem
echo "$domain" > ../config/domain.txt
echo "$hash" > ../config/hash.txt

secret="$(cat ssl/clients/$domain/secret)"
if [ "$secret" != "" ]; then
  echo "Configured secret found for domain, inserting into serverconfig.yaml"
  sed -i -r -e "s/secret: .+/secret: $secret/" ../config/serverconfig.yaml
fi

echo Build package
mkdir builtPackages
pkg ../src/caddyNode/index.js -t linux-node10 -o builtPackages/cadmium-node-server-$domain-$hash

rm ../config/domain.txt
rm ../config/hash.txt

echo done

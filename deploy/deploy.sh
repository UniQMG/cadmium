#!/bin/bash

printf "0). Exit\n"
printf "1). Reinit dev environment\n"
printf "2). Create vertbot node\n"
printf "3). Create new client\n"
printf "4). Delete clients\n"
printf "5). (UniQMG only) Deploy to prod\n"
printf "6). Build server package\n"
printf "Choose: "
read option

if ! [ -e ../config/ ]; then
  mkdir ../config
fi

pkill cfssl # just in case any are left
if [ "$option" = "1" ]; then
  ./build.sh
  if [ "$?" = "73" ]; then
    printf "You'll need to create some nodes first (option 3)\n"
    exit 0
  fi
  cp template/serverconfig.yaml ../config/serverconfig.yaml
  cp ssl/vertbot/db-key.pem ../config/vertbot-db-key.pem
  cp ssl/vertbot/db.pem ../config/vertbot-db.pem
  cp ssl/config.yaml ../config/config.yaml
  cp ssl/ca.pem ../config/ca.pem
  exit 0
fi

if [ "$option" = "2" ]; then
  ./createnode.sh "" "" vertbot
  exit 0
fi

if [ "$option" = "3" ]; then
  ./createnode.sh
  printf "Created node successfully\n"
  exit 0
fi

if [ "$option" = "4" ]; then
  rm -r ./ssl/clients
  exit 0
fi

if [ "$option" = "5" ]; then
  ./build.sh prod
  if [ "$?" = "73" ]; then
    printf "You'll need to create some nodes first (option 3)\n"
  fi
  ./pushprod.sh

  printf "\nUploading Cadmium server node into production...\n"
  ./buildPackage.sh uniq.mg
  if [ "$?" = "1" ]; then
    echo "Failed to build package, not pushing."
    echo "Rebuilding development mode, reinit dev (option 1) recommended."
    ./build.sh
    exit 0
  fi

  echo "Rebuilding development mode"
  ./build.sh

  domain=uniq.mg
  hash=$(git rev-parse HEAD | head -c 7)
  echo "Uploading built package to production"
  cat builtPackages/cadmium-node-server-$domain-$hash | ssh UniQMG@uniq.mg "cat > cadmium-node-server-$domain-$hash"
  echo "Upload done, restarting node..."
  ssh UniQMG@uniq.mg "pm2 delete Caddynode"
  ssh UniQMG@uniq.mg "chmod +x cadmium-node-server-$domain-$hash"
  ssh UniQMG@uniq.mg "pm2 start cadmium-node-server-$domain-$hash --name Caddynode"
  ssh UniQMG@uniq.mg "pm2 save"

  echo "Done"
  exit 0
fi

if [ "$option" = "6" ]; then
  ./buildPackage.sh
  echo "Restoring default serverconfig"
  cp template/serverconfig.yaml ../config/serverconfig.yaml
  exit 0
fi

if [ "$option" = "0" ]; then
  printf "Goodbye\n"
  exit 0
fi

printf "Unknown option $option"

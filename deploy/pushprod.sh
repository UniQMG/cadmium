echo "Pushing..."
git push prod
ssh UniQMG@uniq.mg "cd /var/bots/Cadmium && git reset --hard"

echo "Uploading built files..."
ssh UniQMG@uniq.mg "mkdir -p /var/bots/Cadmium/config/"
cat ssl/config.yaml        | ssh UniQMG@uniq.mg "cat > /var/bots/Cadmium/config/config.yaml"
cat ssl/vertbot/db-key.pem | ssh UniQMG@uniq.mg "cat > /var/bots/Cadmium/config/vertbot-db-key.pem"
cat ssl/vertbot/db.pem     | ssh UniQMG@uniq.mg "cat > /var/bots/Cadmium/config/vertbot-db.pem"
cat ssl/ca.pem             | ssh UniQMG@uniq.mg "cat > /var/bots/Cadmium/config/ca.pem"

ssh UniQMG@45.55.9.207 "pm2 restart Cadmium"

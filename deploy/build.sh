#!/bin/bash
# This script builds various assets needed by Cadmium, but does not deploy them.
# Prompts for information when required, then keeps it for future reference.
if [ "$1" = "prod" ]; then
  ./createnode.sh uniq.mg 0 vertbot
else
  ./createnode.sh localhost 0 vertbot
fi
domains=$(ls -d ssl/clients/*/ 2> /dev/null)

if [ -z "$domains" ]; then
  echo "No backend nodes to distribute, please create a node first"
  exit 73
fi

if ! [ -d ssl/vertbot ]; then
  echo "Vertbot server not built"
  exit 1
fi

# Get the bot token from file or the user

tokenname="Development"
tokenfile=ssl/devtoken
if [ "$1" = "prod" ]; then
  tokenname="Production"
  tokenfile=ssl/prodtoken
  echo "--- WARNING: YOU ARE BUILDING FOR PRODUCTION ---"
fi

token=$(cat $tokenfile);
tokenRegex='^[A-Za-z0-9_.-]{59}$'
while ! [[ "$token" =~ $tokenRegex ]]; do
  printf "$tokenname bot token:"
  read token
done
printf $token > $tokenfile

# Generate node configuration info from existing clients
> ssl/nodes
for dirname in $domains; do
  if [ "$1" = "prod" ] && [ "$dirname" = "ssl/clients/localhost/" ]; then
    echo skipping localhost
  else
    echo "  - name:" $(cat "$dirname"domain) >> ssl/nodes
    echo "    url: https://$(cat "$dirname"domain):8443" >> ssl/nodes
    echo "    secret: $(cat "$dirname"secret)" >> ssl/nodes
    echo "    cert: file(./config/ca.pem)" >> ssl/nodes
    echo "    pipelineLimit: $(cat "$dirname"pipelineLimit)" >> ssl/nodes
    echo "    enabled: true" >> ssl/nodes
  fi
done

# Stick all the data into the template config.yaml
sed \
  -e '/\${NODES}/{r ssl/nodes' -e 'd}' \
  -e "s/\${BOT_TOKEN}/$token/" \
  -e "s/\${VERTBOT_SECRET}/$(cat ssl/vertbot/secret)/" \
  template/config.yaml > ssl/config.yaml
rm ssl/nodes

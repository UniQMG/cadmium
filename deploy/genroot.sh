#!/bin/bash
if [ ! -e ssl/ ]; then
  printf "Generating new root certificate\n"
  mkdir ssl
  CN=Cadmium
  C=US
  L=Location
  O="UniQMG's random projects"
  OU=Cadmium
  ST=State
  sed \
    -e "s/\${CN}/$CN/" \
    -e "s/\${C}/$US/" \
    -e "s/\${L}/$L/" \
    -e "s/\${O}/$O/" \
    -e "s/\${OU}/$OU/" \
    -e "s/\${ST}/$ST/" \
    template/csr_ca.json > ssl/csr_ca.json

  cp template/csr_ca.json ssl/csr_ca.json

  key=$(hexdump -n 16 -e '4/4 "%08X"' /dev/random)
  sed -e "s/\${key}/$key/" template/config-ca.json > ssl/config-ca.json
  sed -e "s/\${key}/$key/" template/config-client.json > ssl/config-client.json
  cfssl gencert -initca ssl/csr_ca.json | cfssljson -bare ssl/ca
else
  echo "ssl folder already exists, not generating new root cert."
fi

const PipelineExecutorService = require('./modules/Pipeline/PipelineExecutorService');
const DatabaseService = require('./modules/Database/DatabaseService.js');
const PipelineService = require('./modules/Pipeline/PipelineService.js');
const VertbotService = require('./modules/Vertbot/VertbotService.js');
const ProfilePicUpdater = require('./modules/ProfilePicUpdater');
const ConfigLoader = require('./modules/ConfigLoader');
const Logger = require('./modules/Logger');
const Bot = require('./modules/Bot/Bot');

process.on('unhandledRejection', (ex) => {
  if (!ex || ex.handled) return; // Instacord workaround
  Logger.createLogger('unhandledRejection').fatal(ex);
  process.exit(-1);
});

//process.on('warning', e => console.warn(e.stack));
module.exports = class Cadmium {
  /**
   * Starts the Cadmium app
   */
  async start() {
    this.configLoader = new ConfigLoader('./config/config.yaml');
    this.logger = Logger.createLogger('cadmium');

    this.pipelineService = new PipelineService(
      Logger.createLogger('PipelineService'),
      this.configLoader.get('pipeline.nodes'),
      this.configLoader.hash()
    );

    let conns = await this.pipelineService.updateConnections();
    this.logger.info('Connections', conns);
    this.vertbotService = new VertbotService(
      Logger.createLogger('VertbotService'),
      this.configLoader
    );

    this.executorService = new PipelineExecutorService(
      this.pipelineService,
      Logger.createLogger('PipelineExecutorService'),
      this.vertbotService
    );

    this.databaseService = new DatabaseService(
      this.configLoader.get('database.host'),
      this.configLoader.get('database.dbname')
    );
    await this.databaseService.connect();

    this.bot = new Bot(
      Logger.createLogger('Bot'),
      this.configLoader,
      this.databaseService,
      this.pipelineService,
      this.executorService,
      this.vertbotService
    );

    this.pfpUpdater = new ProfilePicUpdater(
      this.bot,
      Logger.createLogger('ProfilePicUpdater'),
      this.configLoader.get('avatarFolder')
    );
    this.pfpUpdater.startUpdating();

    this.logger.info(`Cadmium ${this.configLoader.version()}`);
    this.processQueue();
  }

  async processQueue() {
    while(true) {
      if (!await this.executorService.processQueue(this.bot)) {
        await new Promise(res => setTimeout(res, 1000));
      }
    }
  }
}

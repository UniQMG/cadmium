const express = require('express');
const yaml = require('js-yaml');
const path = require('path');
const fs = require('fs');
const app = express();

const config = yaml.safeLoad(fs.readFileSync(
  path.join(__dirname, '../../config/serverconfig.yaml'),
  'utf8'
));

// Attempt to determine version based on version file or git hash
try {
  let hashpath = path.join(__dirname, '../../config/hash.txt');
  if (fs.existsSync(hashpath)) {
    var latesthash = fs.readFileSync(hashpath, 'utf8').trim();
  } else {
    var latesthash = require('child_process')
      .execSync('git rev-parse HEAD')
      .toString().trim().slice(0, 7);
  }
  console.log("Version " + latesthash.trim());
} catch(ex) {
  console.log("Warning: Unable to determine version: " + ex);
}

if (fs.existsSync(config.node.cert) && fs.existsSync(config.node.key)) {
  console.log("Using external cert in " + config.node.cert);
  var validdomain = null;
  var cert = fs.readFileSync(config.node.cert, 'utf8');
  var key = fs.readFileSync(config.node.key, 'utf8');
} else {
  console.log("Using packaged cert");
  // Try and load a text file containing a splash about
  // valid domains for this bundled executable
  try {
    var validdomain = fs.readFileSync(
      path.join(__dirname, '../../config/domain.txt'),
      'utf8'
    );
  } catch(ex) {
    var validdomain = null;
  }
  var cert = fs.readFileSync(
    path.join(__dirname, '../../config/caddynode-db.pem'),
    'utf8'
  );
  var key = fs.readFileSync(
    path.join(__dirname, '../../config/caddynode-db-key.pem'),
    'utf8'
  );
}

const https = require('https').createServer({ key, cert }, app);
https.listen(config.node.listen, () => {
  if (validdomain) {
    console.log("Cadmium node online for " + validdomain.trim());
    return;
  }
  console.log("Cadmium node online");
});

const createPipeline = require('../pipeline/parser');

app.use(require('express-basic-auth')({
  users: { caddy: config.node.secret },
  unauthorizedResponse: (req) => {
    return "401 Unauthorized"
  }
}));

const pipelineLimit = config.node.pipelineLimit;
const elementLimit = config.node.elementLimit;
var pipelinesProcessing = 0;

app.get('/', (req, res) => {
  res.send({
    pipelinesProcessing,
    version: latesthash,
    pipelineLimit,
    elementLimit,
    ready: true
  });
});

var pipelineCount = 0;
app.get('/pipeline', (req, res) => {
  if (pipelinesProcessing >= pipelineLimit) {
    // Too many requests
    res.status(429).send({
      text: 'No available pipeline slots',
      pipelinesProcessing,
      action: 'error',
      pipelineLimit
    });
    return;
  }

  if (!req.query.pipeline) {
    res.status(400).send({
      text: 'No pipeline specified',
      action: 'error'
    });
    return;
  }

  if (req.query.pipeline.split('|').length >= elementLimit) {
    res.send({
      text: 'Pipeline too long',
      action: 'error'
    });
    return;
  }

  var id = ++pipelineCount;
  console.log("Pipeline #" + id + ": \"" + req.query.pipeline + "\"");

  res.status(200);
  res.setHeader('Content-Type', 'application/json');
  var lastSentStatus = Date.now();
  pipelinesProcessing++;
  var packetCount = 0;
  var _pipeline;

  let statusInterval;
  var requestEnded = false;
  req.on('close', function(err) {
    requestEnded = true;
    clearInterval(statusInterval);
    if (_pipeline)
      _pipeline.terminate("SIGHUP");
  });


  function resetStatusInterval() {
    clearInterval(statusInterval);
    statusInterval = setInterval(() => writeStatus(res), 5000)
  }
  resetStatusInterval();

  function writeStatus(res) {
    if (requestEnded) return false;
    if (res.finished) return false;
    try {
      let noBackpressure = res.write(JSON.stringify({
        action: 'status',
        text: _pipeline.status()
      }) + '\n');
      if (!noBackpressure) {
        clearInterval(statusInterval);
        res.once('drain', resetStatusInterval)
      }
    } catch(ex) {
      if (ex.code == 'ERR_STREAM_WRITE_AFTER_END')
        return;
      throw ex;
    }
  }

  const dataLimit = req.query.dataLimit || Infinity;
  createPipeline(req.query.pipeline, {
    sendData: (data) => {
      packetCount++;
      if (packetCount > 5 && Date.now() > lastSentStatus+5000) {
        resetStatusInterval();
        lastSentStatus = Date.now();
        packetCount = 0;
        writeStatus(res);
      }

      if (res.finished) return [false, res];
      if (typeof data === 'object' && data)
        return [res.write(JSON.stringify(data) + '\n'), res];

      return [
        res.write(JSON.stringify({ message: data.toString() }) + '\n'),
        res
      ];
    }
  }).then(pipeline => {
    _pipeline = pipeline;

    return new Promise((resolve, reject) => {
      let killTimeout;
      function resetTimeout() {
        clearTimeout(killTimeout);
        killTimeout = setTimeout(() => {
          reject(new Error("Pipeline timed out"));
          pipeline.terminate("Pipeline timed out");
          setTimeout(() => clearInterval(pipeline.statusInterval), 2000);
        }, 10000);
      }
      resetTimeout();

      let dataProcessed = 0;
      pipeline.forEach(element => {
        element.on('read-data', length => {
          resetTimeout();
        });
        element.on('write-data', length => {
          dataProcessed += length;
          if (dataProcessed > dataLimit) {
            reject(new Error("Data limit exceeded"));
            pipeline.terminate("Data limit exceeded");
            return;
          }
          resetTimeout();
        });
        element.on('terminated', ex => {
          writeStatus(res);
          var extraError = "";
          var isytdl = /^input (youtube|ytsearch)/.test(element.name);
          if (isytdl && /ERROR/.test(ex.toString())) {
            extraError = "This appears to be a youtube-dl issue. "
              + "Please check you have a valid video link or that "
              + "the search query returns valid results. Please don't "
              + "submit bug reports to youtube-dl's issue tracker.";
          }
          reject(new Error(
            `Pipeline object \`${element.name}\` terminated: ` +
            '```' + ex.toString() + '```' + extraError
          ));
        });
      });

      clearTimeout(killTimeout);
      writeStatus(res);
      pipeline.onEnd(() => {
        writeStatus(res);
        resolve();
      });
    })
  }).catch(ex => {
    console.log("Pipeline #" + id + " errored: ", ex);
    res.write(JSON.stringify({ action: 'error', text: ex.message }) + '\n');
  }).then(() => {
    console.log("Pipeline #" + id + " finished.");
    pipelinesProcessing--;
    if (requestEnded) return;
    res.write('\n');
    res.end();
  });
});

const Connection = require('./Connection');
const PipelineQueue = require('./PipelineQueue');

module.exports = class PipelineService {
  /**
   * @param {Logger} logger
   * @param {ConnectionDef} nodes
   * @param {String} hash the git version hash for endpoints to supply
   */
  constructor(logger, nodes, hash) {
    this.logger = logger;
    /** @member {Connection[]} */
    this.connections = nodes.filter(node => node.enabled).map(node => {
      return new Connection(
        node.name,
        node.url,
        node.secret,
        node.cert,
        hash
      );
    });
    /** @member {Map.<String, PipelineQueue>} */
    this.queues = new Map();
    this.iter = this.queues.entries();
  }

  /**
   * Updates every connection and returns their statuses
   * @returns {ConnectionStatus[]}
   */
  async updateConnections() {
    return await Promise.all(this.connections.map(conn => conn.getStatus()));
  }

  /**
   * Reserves a pipeline slot for usage. The slot is locked and must be unlocked
   * after usage to allow it to be reused.
   * @returns {PipelineSlot}
   */
  async getPipelineSlot() {
    for (let conn of this.connections) {
      let slot = await conn.getOpenSlot();
      if (!slot) continue;

      let locked = slot.lock();
      if (!locked) continue;

      return slot;
    }
  }

  /**
   * Makes or retrieves a pipeline queue by ID
   * @param {string} id
   */
  async getPipelineQueue(id) {
    if (this.queues.has(id))
      return this.queues.get(id);
    let queue = new PipelineQueue(id);
    this.queues.set(id, queue);
    return queue;
  }

  /**
   * Returns the next pipeline suitable for processing.
   * returns null if no pipelines are available
   * @return {PipelineQueue}
   */
  async getNextPipeline() {
    if (this.queues.size == 0) return null;

    var {value, done} = this.iter.next();
    if (done) {
      this.iter = this.queues.entries();
      var {value, done} = this.iter.next();
    }

    do {
      if (value) {
        let [index, queue] = value;
        if (queue && await queue.peekPipeline() != null)
          return queue;
      }

      var {value, done} = this.iter.next();
    } while(!done);
    return null;
  }
}

/**
 * A pipeline slot is an arbitrary mutex used for controlling
 * access to pipeline servers. A pipeline job should not use
 * a pipeline slot unless the slot has been locked to the job.
 * When the job is complete, the pipeline slot should be released.
 */
module.exports = class PipelineSlot {
  constructor(connection) {
    /** @member {Connection} connection */
    this.connection = connection;
    /** @member {boolean} processing */
    this.processing = false;
    this.reserved = false;
  }

  isLocked() {
    return this.processing;
  }

  async process(pipeline) {
    this.connection.executePipeline(pipeline);
  }

  /**
   * Gets the connection this slot is associated with.
   */
  getConnection() {
    return this.connection;
  }

  /**
   * Locks the slot
   * @returns {boolean} if the lock was successful
   */
  async lock() {
    if (this.processing) return false;
    this.processing = true;
    return true;
  }

  /**
   * Unlocks the slot
   */
  async release() {
    this.processing = false;
  }
}

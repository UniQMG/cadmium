const PacketHandler = require('./PacketHandler');
const split2 = require('split2');

module.exports = class PipelineExecutorService {
  /**
   * @param {PipelineService} pipelineService the pipeline service to execute pipelines for
   * @param {VertbotService} vertbotService the vertbot service to use
   * @param {Logger} logger
   */
  constructor(pipelineService, logger, vertbotService) {
    this.pipelineService = pipelineService;
    this.logger = logger;
    this.vertbotService = vertbotService;
    /**
     * Maps guild IDs to currently running pipelines
     * @member Map.<string, PipelineExecutor[]>
     */
    this._running = new Map();
  }

  /**
   * @typedef {Object} PipelineExecutor
   * @param {PipelineJob} pipelineJob the pipeline job being executed
   * @param {Promise} promise a promise that resolves when the pipeline finishes
   * @param {function} stop stops the running pipeline
   */

  /**
   * Returns the pipelines running for the given guild
   * @param {string} id guild id
   * @returns {PipelineExecutor[]}
   */
  getGuildPipelineExecutors(id) {
    return [...(this._running.get(id) || [])];
  }

  /**
   * Returns the number of running pipelines
   * @param {number}
   */
  getConcurrentPipelineCount() {
    let count = 0;
    this._running.forEach(executors => {
      count += executors.length;
    });
    return count;
  }

  /**
   * Processes the next item in the queue. Returns false if there's nothing
   * available in in the queue to process, or if processing fails.
   * @param {Bot} bot the bot to use while processing action packets
   * @return {boolean} if an item in the queue was successfully processed.
   */
  async processQueue(bot) {
    let pipelineQueue = await this.pipelineService.getNextPipeline();
    if (!pipelineQueue) return false;


    let slot = await this.pipelineService.getPipelineSlot();
    if (!slot) return false;

    let pipeline = await pipelineQueue.consumePipeline();
    if (!pipeline) {
      await slot.release();
      return false;
    }

    let runArray = this._running.get(pipeline.guild) || [];
    this._running.set(pipeline.guild, runArray);

    this.logger.info("Execute pipeline", pipeline, slot);
    let channel = bot.getClient().channels.get(pipeline.channel);
    let pipecmd = pipeline.pipeline.replace(/``/, '`\u200b`').slice(-200);

    let exec = this.executePipeline(pipeline, bot, slot, this.vertbotService);
    runArray.push(exec);

    let msgPromise = channel.send(`Running pipeline \`\`${pipecmd}\`\``);
    try {
      await exec.promise;
      await msgPromise;
    } catch(ex) {
      let msg = ex.message.indexOf('\n') !== -1
        ? `\`\`\`${ex.message}\`\`\``
        : `\`${ex.message}\`.`;
      await channel.send(`Pipeline \`\`${pipecmd}\`\` failed: ${msg}`);
      this.logger.info("Pipeline failed", pipeline, ex);
    } finally {
      runArray.splice(runArray.indexOf(exec), 1);
    }
    await slot.release();
    return true;
  }

  /**
   * @param {PipelineJob} job pipeline job to execute
   * @param {Bot} bot bot to execute the pipeline for
   * @param {PipelineSlot} slot slot to execute pipeline in
   * @param {VertbotService} vertbotService
   * @returns {PipelineExecutor}
   */
   executePipeline(job, bot, slot, vertbotService) {
    let stopped = false;
    let pipelineExecutor = {
      pipelineJob: job,
      promise: null,
      stop: () => stopped = true
    };

    pipelineExecutor.promise = new Promise(async (resolve, reject) => {
      let channel = bot.getClient().channels.get(job.channel);

      let request = slot.getConnection().makeRequest('/pipeline', {
        pipeline: job.pipeline,
        dataLimit: 524288000
      });

      let packetHandler = new PacketHandler(bot, job, vertbotService);

      request.on('response', async res => {
        if (res.statusCode != 200) {
          reject(new Error(`Unexpected response code: ${res.statusCode}.`));
          res.destroy();
        }

        pipelineExecutor.stop = function() {
          reject(new Error("Stopped"));
          res.destroy();
        }
        if (stopped) pipelineExecutor.stop();

        for await (let line of request.pipe(split2())) {
            if (line.trim().length == 0) return;
            try {
              if (stopped) return;
              await packetHandler.handlePacket(JSON.parse(line));
            } catch(ex) {
              reject(ex);
              res.destroy();
            }
        }
      });
      request.on('error', err => {
        channel.send(`Pipeline error: \`${err.message}\`.`);
        reject(err);
      });
      request.on('end', async () => {
        channel.send(`Pipeline finished.`);
        await packetHandler.finalize(channel);
        resolve();
      });
    });

    return pipelineExecutor;
  }
}

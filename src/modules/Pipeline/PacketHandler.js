const ReplyPacketHandler = require('./handlers/Reply');
const UploadPacketHandler = require('./handlers/Upload');
const VertbotPacketHandler = require('./handlers/Vertbot');
const VoicechatPacketHandler = require('./handlers/Voicechat');

module.exports = class PacketHandler {
  /**
   * @param {Bot} bot
   * @param {PipelineJob} pipe
   * @param {VertbotService} vertbotService
   */
  constructor(bot, pipe, vertbotService) {
    this.bot = bot;
    this.pipe = pipe;
    this.reply = new ReplyPacketHandler();
    this.upload = new UploadPacketHandler();
    this.vertbot = new VertbotPacketHandler(pipe, vertbotService);
    this.voicechat = new VoicechatPacketHandler(bot, pipe);
  }

  /**
   * @typedef {Object} Packet
   * @property {string} action
   * @property {?Buffer} data
   * @property {?string} text
   * @property {?string} format upload/audio format
   */

  /**
   * @typedef {Packet} RichPacket
   * @property {Discord~Channel} channel channel the pipeline was requested in
   * @property {Discord~Guild} guild guild the pipeline was requested in
   * @property {Discord~User} user user who requested the pipeline
   */

  /**
   * @param {Packet} packet
   */
  async handlePacket(packet) {
    let client = this.bot.getClient();
    packet.channel = client.channels.get(this.pipe.channel);
    packet.guild = packet.channel.guild;
    packet.user = packet.guild.members.get(this.pipe.user);
    if (packet.data) packet.data = Buffer.from(packet.data);

    switch (packet.action) {
      case 'reply': await this.reply.handle(packet); break;
      case 'upload': await this.upload.handle(packet); break;
      case 'vertbot': await this.vertbot.handle(packet); break;
      case 'voicechat': await this.voicechat.handle(packet); break;
      case 'info': break; // not going to handle this for now
      case 'error': throw new Error(packet.text);
    }
  }

  /**
   * Finalizes packet handlers, flushing data to discord, etc.
   * @param {Discord~Channel} channel channel to flush data to
   */
  async finalize(channel) {
    await this.upload.upload(channel);
    await this.vertbot.finalize();
    await this.voicechat.finalize();
  }
}

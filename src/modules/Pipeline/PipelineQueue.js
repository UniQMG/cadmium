const PipelineJob = require('./PipelineJob');

module.exports = class PipelineQueue {
  /**
   * @param {string} id a unique id for the queue, usually a discord guild id
   */
  constructor(id) {
    this.id = id;
    this._queue = [];
  }

  /**
   * Adds a new pipeline to the tail of the queue
   * @param {PipelineJob} pipeline
   */
  async add(pipeline) {
    if (!(pipeline instanceof PipelineJob))
      throw new Error('Expected a PipelineJob');
    this._queue.push(pipeline);
  }


  /**
   * Adds a new pipeline to the head of the queue
   * @param {PipelineJob} pipeline
   */
  async addHead(pipeline) {
    if (!(pipeline instanceof PipelineJob))
      throw new Error('Expected a PipelineJob');
    this._queue.unshift(pipeline);
  }

  /**
   * Returns the length of the queue
   * @returns {Number}
   */
  async length() {
    return this._queue.length;
  }

  /**
   * Returns the queue as an array.
   * This array is not backed by the current queue.
   * @returns {PipelineJob[]}
   */
  async array() {
    return [...this._queue];
  }

  /**
   * Returns a job at the given position
   * @returns {PipelineJob}
   */
  async get(index) {
    return this._queue[index];
  }

  /**
   * Removes a job at the given position
   * @returns {boolean} success
   */
  async remove(index) {
    return !!this._queue.splice(index, 1)[0];
  }

  /**
   * Returns the next element in the queue, may return null.
   * @returns {PipelineObject}
   */
  async peekPipeline() {
    return this._queue[0] || null;
  }

  /**
   * Consumes and returns a pipeline, removing it from the queue, may return null.
   * @returns {PipelineObject}
   */
  async consumePipeline(pipeline) {
    return this._queue.splice(0, 1)[0];
  }
}

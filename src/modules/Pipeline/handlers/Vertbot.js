module.exports = class VertbotPacketHandler {
  /**
   * @param {PipelineJob} pipelineJob pipeline to give information to vertbot about
   * @param {VertbotService} vertbotService
   */
  constructor(pipelineJob, vertbotService) {
    this.pipelineJob = pipelineJob;
    this.vertbotService = vertbotService;
    this.storageProvider = null;
    this.cancelled = false;
  }

  /**
   * @param {RichPacket} richPacket
   */
  async handle(richPacket) {
    if (this.cancelled) throw new Error("Pipeline cancelled");
    if (!this.storageProvider) {
      this.storageProvider = await this.vertbotService.createVertbotSong(
        this.pipelineJob,
        () => this.cancelled = true
      );
    }

    await this.storageProvider.append(richPacket.data);
  }

  finalize() {
    if (this.storageProvider)
      this.storageProvider.close();
  }
}

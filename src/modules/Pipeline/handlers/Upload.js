module.exports = class UploadPacketHandler {
  constructor() {
    this.audioFormat = null;
    this.audioBuffer = [];
  }

  /**
   * @param {RichPacket} richPacket
   */
   async handle(richPacket) {
    this.audioFormat = richPacket.format || this.audioFormat;
    let thisSize = this.audioBuffer.reduce((size, buf) => size + buf.length, 0);
    if (thisSize + richPacket.data.length > 8 * 1024 * 1024)
      throw new Error("Upload limit reached");
    this.audioBuffer.push(richPacket.data);
  }

  /**
   * Flushes the current audio buffer to a discord upload.
   * @param {Discord~Channel} channel the channel to upload to
   * @returns {boolean} if the upload was successful (false if no audio to upload)
   */
  async upload(channel) {
    if (this.audioBuffer.length == 0) return false;
    await channel.send({
      file: {
        name: 'audio.' + this.audioFormat,
        attachment: Buffer.concat(this.audioBuffer)
      }
    });
    this.audioBuffer.length = 0;
    return true;
  }
}

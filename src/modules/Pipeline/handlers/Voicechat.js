const through2 = require('through2');
module.exports = class VoicechatPacketHandler {
  /**
   * @param {Bot} bot
   * @param {PipelineJob} pipelineJob
   */
  constructor(bot, pipelineJob) {
    this.bot = bot;
    this.pipelineJob = pipelineJob;
    this.voiceConnection = null;
    this.voiceStream = through2();
  }

  /**
   * @param {RichPacket} richPacket
   */
  async handle(packet) {
    if (!this.voiceConnection) {
      let user = this.bot.getClient().users.get(this.pipelineJob.user);
      let guild = this.bot.getClient().guilds.get(this.pipelineJob.guild);
      let member = guild.member(user);
      if (!member.voiceChannel)
        throw new Error("No voice channel to join, please join one.");

      this.voiceConnection = await member.voiceChannel.join();
      this.voiceConnection.playStream(this.voiceStream);
    }
    if (!this.voiceStream.write(packet.data)) {
      await new Promise(resolve => this.voiceStream.once('drain', resolve));
    }
  }

  /**
   *
   */
  async finalize() {
    if (this.voiceConnection)
      this.voiceConnection.disconnect()
  }
}

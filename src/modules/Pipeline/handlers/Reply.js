module.exports = class ReplyPacketHandler {
  constructor() {
    this.message = null;
    this.text = '';
  }

  /**
   * @param {RichPacket} richPacket
   */
  async handle(richPacket) {
    this.text += richPacket.text;
    if (this.message) {
      await this.message.edit(this.escape(this.text));
    } else {
      this.message = await richPacket.channel.send(this.escape(this.text));
    }
  }

  /**
   * Escapes text with triple backticks
   * @param {string} text text to escape
   * @returns {string} escaped text
   */
  escape(text) {
    let escapedText = text
      .replace(/`/g,'\u200b`')
      .replace(/`$/, '`\u200b')
      .slice(-1994);
    return '```' + escapedText + '```';
  }
}

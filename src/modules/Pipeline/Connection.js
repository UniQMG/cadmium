const PipelineSlot = require('./PipelineSlot');
const request = require('request');

module.exports = class Connection {
  /**
   * @param {String} name the friendly name of the connection
   * @param {String} url the endpoint url, including port and https://
   * @param {String} secret the preshared secret
   * @param {String} cert the public cert in use by the endpoint
   * @param {Number} slots the number of pipelines slots for this connection
   * @param {String?} version the expected git hash of the endpoint
   */
  constructor(name, url, secret, cert, version) {
    this.name = name;
    this.url = url;
    this.secret = secret;
    this.cert = cert;
    this.version = version;
    /** @member {?ConnectionStatus} */
    this._lastStatus = null;
    this.slots = [];
  }

  /**
   * Returns an open processing slot for this Connnection
   * @returns {PipelineSlot}
   */
  async getOpenSlot() {
    let { status, limit } = await this.getStatus();
    if (status == 'offline') return null;

    while (this.slots.length < limit)
      this.slots.push(new PipelineSlot(this));

    for (let slot of this.slots) {
      if (!slot.isLocked()) {
        return slot;
      }
    }

    return null;
  }

  /**
   * @typedef {Object} ConnectionStatus
   * @property {string} status - 'online' or 'offline'
   * @property {?number} active - number of active pipelines
   * @property {?number} limit - limit on number of active pipelines
   */

  /**
   * Agent and authorization objects for requests to this node
   * @returns {Object}
   */
  _getRequestOptions() {
    return {
      agentOptions: { ca: this.cert },
      auth: {
        user: 'caddy',
        pass: this.secret
      }
    }
  }

  /**
   * Gets the last reported status of the node, or null
   * @returns {?ConnectionStatus}
   */
  getLastStatus() {
    return this._lastStatus;
  }

  /**
   * Makes an arbitrary get request to the pipeline server
   * @param {String} path the path to get (e.g. /pipeline)
   * @param {Object.<String, String>} query query parameters to include
   * @returns {Request}
   */
  makeRequest(url, query) {
    let queryString = Object.entries(query || {})
      .map(([key,value]) => key+'='+encodeURIComponent(value))
      .join('&');
    return request({
      url: this.url + url + (queryString.length > 0 ? '?' + queryString : ''),
      ...this._getRequestOptions()
    });
  }

  /**
   * Gets the current status of the node, and updates the internal _lastStatus
   * @async
   * @returns {ConnectionStatus}
   */
  async getStatus() {
    let status = await new Promise((resolve, reject) => {
      request({
        url: this.url,
        json: true,
        ...this._getRequestOptions()
      }, (err, res, body) => {
        if (err) return resolve({ status: 'offline', info: err.code });
        if (res.statusCode != 200) return resolve({
          info: 'Unexpected response code ' + res.statusCode,
          status: 'offline'
        });
        if (this.version != body.version) return resolve({
          info: `Version mismatch, expected ${this.version} got ${body.version}`,
          status: 'offline'
        });
        resolve({
          status: 'online',
          active: body.pipelinesProcessing,
          limit: body.pipelineLimit
        })
      });
    });
    this._lastStatus = Object.assign({}, status);
    return status;
  }
}

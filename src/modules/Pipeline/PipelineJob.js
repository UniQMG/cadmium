module.exports = class PipelineJob {
  /**
   * @param {string} pipeline the pipeline command
   * @param {string} user discord id of the user who requested the pipeline
   * @param {string} channel channel id the pipeline was requested in
   * @param {string} guild guild id the pipeline was requested in
   */
  constructor(pipeline, user, channel, guild) {
    this.guild = guild;
    this.pipeline = pipeline;
    this.channel = channel;
    this.user = user;
  }
}

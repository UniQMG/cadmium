const bunyan = require('bunyan');

module.exports = class Logger {
  static createLogger(name) {
    return bunyan.createLogger({
      name: name
    });
  }
}

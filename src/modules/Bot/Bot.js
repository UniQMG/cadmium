const Instacord = require('instacord2');
const Commander = require('./Commander');
const PrefixManager = require('./PrefixManager');
const EmbedHelper = require('./EmbedHelper');

module.exports = class Bot {
  /**
   * @param {Logger} logger
   * @param {ConfigLoader} configLoader
   * @param {DatabaseService} databaseService
   * @param {PipelineService} pipelineService
   * @param {ExecutorService} executorService
   * @param {VertbotService} vertbotService
   */
  constructor(logger, configLoader, databaseService, pipelineService, executorService, vertbotService) {
    let token = configLoader.get('bot.token');

    this.instacord = new Instacord();
    this.instacord.memstore();
    this.instacord.load();
    this.instacord.login(token);

    this.root = new Commander();
    this.prefixManager = new PrefixManager(
      configLoader.get('prefix'),
      databaseService.getModel('prefix')
    );
    this.prefixManager.getPrefixCommander(this.instacord).mount(this.root);
    this.embedHelper = new EmbedHelper(configLoader, this);

    this.root.cmd('ping', (sub, msg, actions) => {
      msg.channel.send("Hello, world!");
    });

    require('./commands/general/help')({
      commander: this.root,
      rootCommander: this.root
    });
    require('./commands/general/ping')({
      commander: this.root,
      embedHelper: this.embedHelper
    });
    require('./commands/general/man')({
      commander: this.root,
      docsFolder: configLoader.get('docsFolder')
    });
    require('./commands/general/prefix')({
      commander: this.root,
      prefixManager: this.prefixManager,
      embedHelper: this.embedHelper
    });
    require('./commands/general/info')({
      commander: this.root,
      embedHelper: this.embedHelper,
      prefixManager: this.prefixManager,
      pipelineService,
      executorService,
      vertbotService
    });
    require('./commands/pipeline/pipeline')({
      commander: this.root,
      pipelineService
    });
    require('./commands/pipeline/queue')({
      commander: this.root,
      embedHelper: this.embedHelper,
      pipelineService,
      executorService
    });
    require('./commands/pipeline/stop')({
      commander: this.root,
      embedHelper: this.embedHelper,
      pipelineService,
      executorService
    });
    
    this.root.on('resolutionException', (retval, err, cmd, [msg, actions]) => {
      let incidentId = Date.now();
      logger.fatal("Uncaught error processing message", {
        incidentId: incidentId,
        message: msg.content,
        author: msg.author.id,
        error: err
      });
      err.handled = true;
      msg.channel.send(this.embedHelper.buildSimple(
        'Internal error',
        `Encountered an internal error while processing the command\n` +
        `The bot will now restart to ensure a consistent internal state\n` +
        `This will interrupt any running pipelines`,
        'error',
        [{
          name: 'Incident ID',
          value: this.embedHelper.quote(incidentId),
          inline: true
        }, {
          name: 'Error message',
          value: this.embedHelper.quote(err.message).replace(/ /g, '\u00A0'),
          inline: true
        }]
      )).catch(ex => {
        logger.error("Failed to send error alert message");
      }).then(() => {
        process.exit(-1);
      })
    });
  }

  getClient() {
    return this.instacord.client;
  }
}

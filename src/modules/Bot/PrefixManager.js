const Commander = require('./Commander');

module.exports = class PrefixManager {
  /**
   * @param {string} defaultPrefix the default bot prefix
   * @param {Mongoose~Model} prefix
   */
  constructor(defaultPrefix, Prefix) {
    this._prefixMap = new Map();
    this._defaultPrefix = defaultPrefix;
    this.Prefix = Prefix;
  }

  /**
   * Returns the prefix for a guild
   * @param {string} guild the guild id
   * @returns {Prefix} the prefix
   */
  async getPrefix(guild) {
    let prefix = await this.Prefix.findOne({ guild });
    if (!prefix) return new this.Prefix({ guild });
    return prefix;
  }

  /**
   * Sets the prefix for a guild
   * @param {string} guild the guild id
   * @param {string} prefix the new prefix, blank to reset to default
   */
  async setPrefix(guild, prefix) {
    let prefixModel = await this.getPrefix(guild);
    if (!prefix || prefix.length == 0)
      prefix = this._defaultPrefix;
    prefixModel.prefix = prefix;
    await prefixModel.save();
  }

  /**
   * Mounts and returns commander that enforces prefixes
   * @param {Router} mountOn the router to mount the commander on
   * @returns {Router}
   */
  getPrefixCommander(mountOn) {
    let commander = new Commander();
    mountOn.middleware(async (sub, msg, actions) => {
      msg.prefix = (await this.getPrefix(msg.guild.id)).prefix;
    });
    mountOn.mount((match, msg, actions) => {
      let prefix = msg.prefix;
      let mention1 = '<@' + msg.client.user.id + '>';
      let mention2 = '<@!' + msg.client.user.id + '>';

      if (match.startsWith(mention1)) return mention1;
      if (match.startsWith(mention2)) return mention2;
      if (match.startsWith(prefix)) return prefix;
      return false;
    }, commander);
    return commander;
  }
}

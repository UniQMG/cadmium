/**
 * @param {Object} params
 * @param {Commander} params.commander
 * @param {PrefixManager} params.prefixManager
 * @param {EmbedHelper} params.embedHelper
 */
module.exports = ({commander, prefixManager, embedHelper}) => commander.command({
  name: "prefix",
  description: "change server command prefix",
  usage: "prefix <new prefix>" +
  "\nInvoke without arguments to get current prefix." +
  "\nThe default prefix is 'caddy'."
}, async (sub, msg, actions) => {
  let prefix = sub.trim();
  if (prefix.length > 64) {
    await msg.channel.send(embedHelper.buildBasic(
      'Prefix too long',
      'Maximum length is 64 chars',
      'danger'
    ));
    return;
  }
  await prefixManager.setPrefix(msg.guild.id, prefix);

  let newPrefix = (await prefixManager.getPrefix(msg.guild.id)).prefix;
  await msg.channel.send(embedHelper.buildBasic(
    'Prefix changed',
    "New prefix: `" + newPrefix + "`! ",
    'success'
  ));
});

/**
 * @param {Object} params
 * @param {Commander} params.commander
 * @param {EmbedHelper} params.embedHelper
 */
module.exports = ({ commander, embedHelper }) => commander.command({
  name: "ping",
  description: "prints bot latency to discord gateway",
}, async (sub, msg, actions) => {
  let sendTime = Date.now();
  let latency = Math.floor(msg.client.ping);
  let replymsg = await msg.channel.send(
    embedHelper.buildSimple('Ping', null, 'info', [{
      name: "Gateway latency",
      value: latency + 'ms',
      inline: true
    }, {
      name: "Round-trip message latency",
      value: '(Determining)',
      inline: true
    }])
  );

  let editLag = Date.now() - sendTime;
  await replymsg.edit(
    embedHelper.buildSimple('Ping', null, 'info', [{
      name: "Gateway latency",
      value: latency + 'ms',
      inline: true
    }, {
      name: "Round-trip message latency",
      value: editLag + 'ms',
      inline: true
    }])
  );
});

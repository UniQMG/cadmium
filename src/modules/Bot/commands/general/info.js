/**
 * @param {Object} params
 * @param {Commander} params.commander the commander to add the command to
 * @param {EmbedHelper} params.embedHelper
 * @param {PipelineService} params.pipelineService
 * @param {ExecutorService} params.executorService
 * @param {PrefixManager} params.prefixManager
 * @param {VertbotService} params.vertbotService
 */
module.exports = ({
  commander,
  embedHelper,
  pipelineService,
  executorService,
  prefixManager,
  vertbotService
}) => {
  commander.command({
    name: 'info',
    cmd: /^info|status|about/,
    description: 'bot status information',
    usage: 'info'
  }, async (sub, msg, actions) => {
    const link = "https://discordapp.com/oauth2/authorize"
               + "?scope=bot&permissions=3180544"
               + "&client_id=488092124282224680";
    await msg.channel.send(embedHelper.buildSimple('Info', null, null, [{
      name: "Author",
      value: '**UniQMG**​*#0522*',
      inline: true
    }, {
      name: "Support server link",
      value: `**[discord.gg](https://discord.gg/HTB76e7)**`,
      inline: true
    }, {
      name: "Invite link",
      value: `**[discordapp.com](${link})**`,
      inline: true
    }, /* Row 2 */ {
      name: "Prefix",
      value: (await prefixManager.getPrefix(msg.guild.id)).prefix,
      inline: true
    }, {
      name: "Pipelines",
      value: await executorService.getConcurrentPipelineCount(),
      inline: true
    }, {
      name: "Capacity",
      value: (await pipelineService.updateConnections()).reduce((acc, stat) => {
        if (stat.status == 'offline') return acc;
        return acc + stat.limit;
      }, 0),
      inline: true
    }, /* Row 3 */ {
      name: "Shard",
      value: "1 / 1",
      inline: true
    }, {
      name: "Guild count",
      value: msg.client.guilds.size,
      inline: true
    }, {
      name: "Vertbot connections",
      value: vertbotService.getConnectionCount(),
      inline: true
    }]));
  });
};

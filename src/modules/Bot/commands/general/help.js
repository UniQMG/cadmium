/**
 * @param {Object} params
 * @param {Commander} params.commander
 * @param {Commander} params.rootCommander the base commander, for helptext
 */
module.exports = ({commander, rootCommander}) => commander.command({
  name: "help",
  description: "prints help. 'help help' for more help.",
  usage: "help <command name>"
  + "\nHelp can be used on any command. For example, 'help help' prints this"
  + "\nadvanced usage. To print subcommands in a group, do 'help <subgroup>'."
  + "\nTo print advanced usage for a command in a subgroup, add that command"
  + "\nto the help query (help <subgroup> <command>)."
}, (sub, msg, actions) => {
  if (sub.trim() === "me") return actions.send("Help yourself.");
  let help = rootCommander.generateHelp(sub.trim());
  msg.channel.send(`\`\`\`\n${help}\n\`\`\``);
});

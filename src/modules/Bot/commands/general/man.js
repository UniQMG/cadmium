const path = require('path');
const fs = require('fs');

/**
 * @param {Object} params
 * @param {Commander} params.commander
 * @param {String} params.docsFolder
 */
module.exports = ({commander, docsFolder}) => {
  const docs = {};
  for (let file of fs.readdirSync(docsFolder)) {
    let content = fs.readFileSync(path.join(docsFolder, file), 'utf8');
    docs[file] = {
      content: content,
      pages: content.split('%pagebreak')
    };
  }

  const index = Object.entries(docs).map(([name, doc]) => {
    return `${name}: ${doc.content.length} chars, ${doc.pages.length} pages.`;
  }).join('\n');

  commander.command({
    name: "man",
    description: "prints manual pages.",
    usage: "help <manpage name> <page number (or 'all')>"
    + "\nPrints an advanced help article. use without arguments to list docs."
  }, async (sub, msg, actions) => {
    if (sub.trim().length == 0) {
      await actions.send("Available manual pages: ```" + index + '```');
      return;
    }

    let [manpage, pagenum] = sub.toLowerCase().trim().split(' ');

    let manual = docs[manpage];
    if (!manual) return actions.send("Unknown manual.");

    let pages = manual.pages;
    if (pagenum === 'all') {
      for (let page of pages)
        await actions.send(page);
      return;
    }

    if (pages.length == 1) pagenum = 1;
    if (!pagenum || pagenum.length == 0) {
      await actions.send(`Please specify a page number, 1-${pages.length}.`);
      return;
    }

    pagenum = Number.parseInt(pagenum)-1;
    if (!pages[pagenum]) return actions.send("Unknown page number.");
    await actions.send(pages[pagenum]);
  });
}

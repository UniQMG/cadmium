/**
 * @param {Object} params
 * @param {Commander} params.commander the commander to add the command to
 * @param {EmbedHelper} params.embedHelper
 * @param {PipelineService} params.pipelineService
 * @param {ExecutorService} params.executorService
 */
module.exports = ({
  commander,
  embedHelper,
  pipelineService,
  executorService
}) => commander.command({
  name: 'stop',
  cmd: /^(stop|rm|remove)/,
  description: 'stops a pipeline',
  usage: 'stop <index>\n' +
  'Stops a pipeline at the given index (see \'caddy queue\')\n' +
  'Pass with no arguments to kill the first running pipeline\n' +
  'Pass with argument \'all\' to completely clear the queue'
}, async (sub, msg, actions) => {
  let pipelineQueue = await pipelineService.getPipelineQueue(msg.guild.id);
  let executors = executorService.getGuildPipelineExecutors(msg.guild.id);

  if (sub.trim().startsWith('all') || sub.trim().startsWith('*')) {
    let removed = 0;
    while (await pipelineQueue.get(0) && await pipelineQueue.remove(0))
      removed++;

    for (let executor of executors) {
      executor.stop();
      removed++;
    }

    await msg.channel.send(embedHelper.buildBasic(
      'Stop Pipeline',
      `Cleared ${removed} pipelines`
    ));
    return;
  }

  let index = Number.parseInt(sub.trim());
  if (Number.isNaN(index)) {
    if (executors.length == 0) {
      await msg.channel.send(embedHelper.buildBasic(
        'Stop Pipeline',
        `No pipelines to stop`,
        'danger'
      ));
      return;
    }

    for (let executor of executors)
      executor.stop();

    if (executors.length > 1) {
      let name = executors[0].pipelineJob.pipeline;
      await msg.channel.send(embedHelper.buildBasic(
        'Stop Pipeline',
        `Pipeline ${embedHelper.quote(name)} stopped`,
        'success'
      ));
      return;
    } else {
      await msg.channel.send(embedHelper.buildBasic(
        'Stop Pipeline',
        `Stopped ${executors.length} running pipelines`,
        'success'
      ));
      return;
    }
  }

  let pipeline = await pipelineQueue.get(index);
  if (!pipeline) {
    await msg.channel.send(embedHelper.buildBasic(
      'Stop Pipeline',
      `Pipeline #${index} not found`,
      'danger'
    ));
    return;
  }

  await pipelineQueue.remove(index);
  await msg.channel.send(embedHelper.buildBasic(
    'Stop Pipeline',
    `Pipeline ${embedHelper.quote(pipeline.pipeline)} cancelled.`,
    'success'
  ));
});

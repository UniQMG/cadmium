const PipelineJob = require('../../../Pipeline/PipelineJob');

/**
 * @param {Object} params
 * @param {Commander} params.commander the commander to add the command to
 * @param {PipelineService} params.pipelineService
 */
module.exports = ({commander, pipelineService}) => commander.command({
  name: 'pipeline',
  description: 'creates a new pipeline',
  usage: 'pipeline <args>'
}, async (sub, msg, actions) => {
  if (sub.trim().length == 0) return msg.channel.send(`Please specify a pipeline`);
  let pipelineQueue = await pipelineService.getPipelineQueue(msg.guild.id);
  let pipelineJob = new PipelineJob(
    sub.trim(),
    msg.author.id,
    msg.channel.id,
    msg.guild.id
  );
  await pipelineQueue.add(pipelineJob);

  let escaped = sub.replace(/``/, '`\u200b`').slice(-1900);
  await msg.channel.send(`Pipeline \`\`${escaped}\`\` added to queue!`);
});

/**
 * @param {Object} params
 * @param {Commander} params.commander the commander to add the command to
 * @param {EmbedHelper} params.embedHelper
 * @param {PipelineService} params.pipelineService
 * @param {ExecutorService} params.executorService
 */
module.exports = ({
  commander,
  embedHelper,
  pipelineService,
  executorService
}) => commander.command({
  name: 'queue',
  cmd: /^(queue|list|ls)/,
  description: 'shows the pipeline queue',
  usage: 'queue'
}, async (sub, msg, actions) => {
  let queue = await (await pipelineService.getPipelineQueue(msg.guild.id)).array();
  let executors = executorService.getGuildPipelineExecutors(msg.guild.id);
  let running = executors.map(exec => exec.pipelineJob);
  let maxWidth = queue.length.toString().length;

  function jobToStringEntry(job, i) {
    let leftpad = new Array(Math.max(maxWidth - (''+i).length, 0)).fill(' ').join('');
    return `[${leftpad}${i}] ${job.pipeline}`.replace(/ /g, '\u00A0')
  }
  let mapped = [];

  mapped.push(...running.map((job, i) => jobToStringEntry(job, '>')));
  mapped.push(...queue.map((job, i) => jobToStringEntry(job, i)));

  let lines = [];
  for (var i = 0; i < mapped.length; i++) {
    if (lines.reduce((l,e) => l+e.length, 0) < 1500) {
      lines.push(mapped[i].replace(/(.{100}).+/, '$1...'));
    } else {
      break;
    }
  }
  lines.push(`{ ${ running.length } running, ${queue.length} queued }`);

  let embedText = embedHelper.blockquote(lines.join('\n'));
  await msg.channel.send(embedHelper.buildSimple('Pipeline Queue', embedText));
});

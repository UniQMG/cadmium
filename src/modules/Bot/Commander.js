const Command = require('instacord2/instacord/command');
const Router = require('instacord2/instacord/router');

function pad(string, length) {
  if (string.length >= length)
    return string.substring(0, length);
  return string + " ".repeat(length - string.length);
}

class Commander extends Router {
  /**
   * @typedef {Object} CommandInfo
   * @property {String} name The resolvable name of the command
   * @property {String} description a short description of the command
   * @property {String} usage in-depth description and usage of the command
   * @property {Boolean} group if the command should be reachable exclusively via group
   * @property {Instacord~Resolver} cmd instacord resovler to use (e.g. regex)
   */

  /**
   * Mounts a Commander, which provides passthrough for help generation
   * @param {CommandInfo} info
   * @param {Commander} router: the subcommander to mount
   * @param {...Middleware} middleware standard instacord middleware to mount
   * @return {Commander} this
   */
  commandMount(info, router, ...middleware) {
    this._validateInfo(info);
    if (!(router instanceof Commander)) {
      throw new Error("expected 2nd argument to be a Commander");
    }

    var command = new Command((cmd) => {
      if (info.cmd instanceof RegExp) {
        return (info.cmd.exec(cmd) || [false])[0];
      } else if (cmd.startsWith(info.cmd)) {
        return info.cmd.length;
      }
      return info.group ? false : 0;
    });
    command.helpResolver = new Command(info.cmd);
    middleware.forEach(ware => command.chain(ware));
    command.chain(function() {
      return router.resolve(...arguments);
    });

    if (info.group) {
      this.mounts.set(info.cmd, command);
    } else {
      this.mounts.set(Symbol(), command);
    }

    command.name = info.name;
    command.description = info.description;
    command.subgroup = true;
    command.router = router;

    return this;
  }


  /**
   * Constructs and returns a new Commander mounted on the given path. Basically
   * inline `commandMount(cmd, new Commander());`
   * @override
   * @param {CommandInfo} info
   * @param {...Middleware} middleware The middleware functions to execute
   * @return {Commander} The commander mounted on the path ${cmd}
   */
  group(info, ...middleware) {
    if (typeof cmd === 'function') {
      middleware.unshift(cmd);
      cmd = '';
    }
    var commander = new Commander();
    this.commandMount(info, commander, ...middleware);
    return commander;
  }

  /**
   * Validates and fixes command metadata
   * @param {Object} info command metadata
   * @throws {Error} on error
   */
  _validateInfo(info) {
    if (typeof info.name !== 'string') {
      throw new Error('info.name must be a string');
    }
    if (!info.cmd) info.cmd = info.name;
    if (!info.usage) info.usage = info.name;
    if (!info.description) info.description = "";
  }

  /**
   * Merges another commander's commands into this one.
   * @param {Commander} commander the commander to merge into this one
   * @return {Commander} this
   */
  commandMerge(commander) {
    if (!(commander instanceof Commander)) {
      throw new Error("expected first argument to be a Commander");
    }
    this.mounts = new Map([...this.mounts, ...commander.mounts]);
    return this;
  }

  /**
   * Creates a new command
   * @param {Object} info: keys 'name' 'cmd' 'description' 'usage'
   * @param {...Middleware} middleware standard instacord middleware to mount
   * @return {Commander} this
   */
  command(info, ...middleware) {
    this._validateInfo(info);

    var command = new Command(info.cmd);
    middleware.forEach(ware => command.chain(ware));
    this.mounts.set(info.cmd, command);
    command.name = info.name;
    command.usage = info.usage;
    command.description = info.description;
    command.subgroup = false;
    return this;
  }

  /**
   * Recursively generates help for a command
   * @param {String} command the command to search help for
   * @param {?String} originalCommand same as command. defaults to command.
   * @return {String} the generated help text
   */
  generateHelp(command, originalCommand) {
    if (!originalCommand) originalCommand = command;
    if (!command) command = "";
    let root = this;

    for (let [key, value] of this.mounts.entries()) {
      let result = value.helpResolver
        ? value.helpResolver.test(command)
        : value.test(command);
      if (result !== false) {
        if (value.router) {
          if (!value.router.generateHelp)
            return "No help available for '" + originalCommand + "'";
          return value.router.generateHelp(result.trim(), originalCommand);
        }
        if (value.name) {
          let header = `\n${value.name}\n----------`;
          let body = `${value.description}\nUsage: ${value.usage}`
          return header + '\n' + body;
        }
      }
    }

    // check if there's remaining unparsed command
    if (command.length > 0) {
      return "Unknown command '" + originalCommand + "'.";
    }

    // Generate command help for all routables
    let help = [];
    for (let [key, value] of this.mounts.entries()) {
      if (!value.name) {
        help.push([value._cmd])
      } else if(value.subgroup) {
        help.push([value.name, value.description || 'command group']);
      } else {
        help.push([value.name, value.description]);
      }
    }

    if (help.length == 0) {
      return "No commands in group '" + originalCommand + "'.";
    }

    help = help.filter(([name]) => !!name);
    let maxlength = Math.max(...help.map(([name]) => name.length));
    return help.map(([name, desc]) => {
      return `${pad(name, maxlength)} | ${desc}`
    }).join('\n');
  }
}

module.exports = Commander;

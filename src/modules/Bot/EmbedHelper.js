module.exports = class EmbedHelper {
  /**
   * @param {ConfigLoader} configLoader
   * @param {Bot} bot
   */
  constructor(configLoader, bot) {
    this.configLoader = configLoader;
    this.bot = bot;
  }

  /**
   * Gets a color from a status level
   * @param {string} level 'ok' or 'success' or 'warn' or 'danger' or 'error'
   * @returns {number} color
   */
  getColorForLevel(level) {
    switch (level) {
      default:        return 0x22641e;
      case 'success': return 0x18a300;
      case 'warn':    return 0xec8514;
      case 'danger':  return 0xc9262e;
      case 'error':   return 0x6e0f22;
    }
  }

  /**
   * Builds a basic embed with only plaintext title and a description
   * @param {string} name embed title/header
   * @param {string} text embed text
   * @param {?string} level the message level, see #getColorForLevel
   * @returns {Discord~MessageOptions}
   */
  buildBasic(name, text, level) {
    if (name && name.length > 100) {
      name = name.substring(0, 97) + '..';
    }
    if (text && text.length > 2000) {
      text = text.substring(0, 1997) + '...';
    }

    return {
      embed: {
        author: { name },
        color: this.getColorForLevel(level),
        description: text
      }
    };
  }

  /**
   * Builds a simple embed with only a rich title and a description
   * @param {string} name embed title/header
   * @param {string} text embed text
   * @param {?string} level the message level, see #getColorForLevel
   * @param {?Discord~MessageEmbedField[]} fields optional fields to add
   * @returns {Discord~MessageOptions}
   */
  buildSimple(name, text, level, fields) {
    if (name && name.length > 100) {
      name = name.substring(0, 97) + '..';
    }
    if (text && text.length > 2000) {
      text = text.substring(0, 1997) + '...';
    }

    return {
      embed: {
        author: {
          name: 'Cadmium - ' + name,
          url: 'https://uniq.mg/cadmium',
          icon_url: this.bot.getClient().user.avatarURL
        },
        color: this.getColorForLevel(level),
        footer: {
          icon_url: this.bot.getClient().user.avatarURL,
          text: `Cadmium ${this.configLoader.version()}`
        },
        timestamp: new Date().toISOString(),
        description: text,
        fields
      }
    };
  }

  /**
   * Escapes text and puts it into a ```block quote```
   * @param {String} text
   * @returns {String}
   */
  blockquote(text) {
    return `\`\`\`${(''+text).replace(/```/g, '`\u200b`\u200b`')} \`\`\``;
  }

  /**
   * Escapes text and puts it into a ``quote``
   * @param {String} text
   * @returns {String}
   */
  quote(text) {
    return `\`\`${(''+text).replace(/``/g, '`\u200b`')}\`\``;
  }
}

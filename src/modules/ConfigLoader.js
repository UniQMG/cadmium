const { execSync } = require('child_process');
const ExprParser = require('expr-eval').Parser;
const dot = require('dot-object');
const mkdirp = require('mkdirp');
const yaml = require('js-yaml');
const fs = require('fs');

module.exports = class ConfigLoader {
  /**
   * Creates a new configuration loader
   * @param {string} configFile path to the config file
   */
  constructor(configFile) {
    this._configFile = configFile;
    this._config = yaml.safeLoad(fs.readFileSync(configFile));
    this._package = JSON.parse(fs.readFileSync('./package.json'));
    this._parse(this._config);

    this._version = this._package.version;
    if (!this._version) throw new Error("No version key in package.json");

    try {
      this._hash = execSync('git rev-parse HEAD').toString().trim().slice(0, 7);
    } catch(ex) {
      this._hash = null;
    }
  }

  /**
   * Recursively parses an object in-place, filling in custom strings.
   * file(./path/to/file) -> <contents of file>
   * mkdir(./path) -> ./path, makes directory
   * calc(expr) -> evaluates simple arithmetic with variables from current object
   * @param {Object} object object to parse
   */
  _parse(object) {
    for (var prop in object) {
      if (!object.hasOwnProperty(prop)) continue;
      let value = object[prop];
      if (!value) continue;

      if (typeof value == 'object') {
        this._parse(value);
        continue;
      }

      if (typeof value == 'string') {
        object[prop] = value.replace(/file\((.+?)\)/g, (match, filepath) => {
          if (!fs.existsSync(filepath))
            throw new Error(`Config error: file ${filepath} does not exist.`);
        	return fs.readFileSync(filepath, 'utf8');
        }).replace(/mkdir\((.+?)\)/g, (match, filepath) => {
          mkdirp(filepath);
          return filepath;
        }).replace(/calc\((.+?)\)/g, (match, expr) => {
          let parser = new ExprParser();
          return parser.evaluate(expr, object);
        });
      }
    }
  }

  /**
   * Gets a value from the config, or a default value
   * @throws {Error} if the value is not found and no default is provided
   * @param {string} path the dot-notation path of the value
   * @param {} _default the default value to return
   * @returns {}
   */
  get(path, _default) {
    let value = dot.pick(path, this._config);
    if (typeof value == 'undefined') {
      if (typeof _default == 'undefined')
        throw new Error(`Key ${path} not found in config, no default provided.`);
      return _default;
    }
    return value;
  }

  /**
   * Returns the current git hash, determined at startup.
   * @returns {?string}
   */
  hash() {
    return this._hash;
  }

  /**
   * Attempts to determine the version
   * Usually hardcoded in version.json, plus the git hash if applicable
   * @returns {string}
   */
  version() {
    return `v${this._version} (${this._hash || '[unknown commit]'})`;
  }
}

const mongoose = require('mongoose');

module.exports = class DatabaseService {
  /**
   * @param {String} host mongodb hostname and port url
   * @param {String} dbname the database name to use
   */
  constructor(host, dbname) {
    this.host = host;
    this.dbname = dbname;
    this.db = null;

    /**
     * @type {Map<string, Mongoose~Model>}
     */
    this.types = new Map();
    this.types.set('prefix', require('./schema/Prefix'));
  }

  /**
   * Finds a mongoose model by name
   * @param {String} name
   * @returns {Mongoose~Model}
   * @throws on unknown model name
   */
  getModel(name) {
    if (this.types.has(name))
      return this.types.get(name);
    throw new Error('Unknown model ' + name);
  }

  /**
   * Connects to the database
   * @throws on connection failure
   */
  async connect() {
    mongoose.connect(`mongodb://${this.host}/${this.dbname}`, {
      useNewUrlParser: true
    });

    this.db = mongoose.connection;
    await new Promise((resolve, reject) => {
      this.db.on('error', reject);
      this.db.once('open', resolve);
    });
  }
}

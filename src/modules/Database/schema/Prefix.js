const mongoose = require('mongoose');

/**
 * @class Prefix
 * @property {String} guild discord guild id
 * @property {String} prefix guild prefix
 */
const prefixSchema = new mongoose.Schema({
  guild: {
    type: String,
    required: true,
    unique: true,
    match: /^\d{18}$/
  },
  prefix: {
    type: String,
    default: 'caddy',
    minlength: [1, 'Prefix too short'],
    maxlength: [64, 'Prefix cannot exceed 64 characters']
  }
});

module.exports = mongoose.model('Prefix', prefixSchema);

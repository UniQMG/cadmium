const uuid = require('uuid/v4');
const path = require('path');
const fs = require('fs');

module.exports = class FileStorageProvider {
  /**
   * @param {string} storageDirectory
   * @param {string} extension
   */
  constructor(storageDirectory, extension) {
    this.uuid = uuid();
    this.writeStream = null;
    this.path = path.join(
      storageDirectory,
      this.uuid + (extension ? '.'+extension : '')
    );
    this.writeStream = fs.createWriteStream(this.path);

    this.finished = false;
    this.writeStream.on('finish', () => {
      this.finished = true;
    });
  }

  /**
   * Appends data to the file
   * @param {Buffer} data
   * @returns {boolean}
   */
  async append(data) {
    return await new Promise((resolve, reject) => {
      function doReject(ex) { reject(ex) }
      this.writeStream.setMaxListeners(20);
      this.writeStream.on('error', doReject);
      this.writeStream.write(data, (err) => {
        this.writeStream.removeListener('error', doReject);
        if (err) return reject(err);
        resolve();
      });
    });
  }

  /**
   * Finishes writing to the file
   * @returns {boolean} if the stream was closed (false if already closed)
   */
  close() {
    if (this.finished) return false;
    this.writeStream.end();
    this.finished = true;
    return true;
  }

  /**
   * Calls a callback when the stream is done processing. May call immediately,
   * if already finished.
   * @param {function} callback
   */
  onClose(callback) {
    if (this.finished) return callback();
    this.writeStream.on('finish', callback);
  }
}

const FileStorageProvider = require('./FileStorageProvider');
const express = require('express');
const WebSocket = require('ws');
const https = require('https');
const path = require('path');
const fs = require('fs');

module.exports = class VertbotService {
  /**
   * @param {Logger} logger
   * @param {ConfigLoader} configLoader
   */
  constructor(logger, configLoader) {
    this.mediaPort = configLoader.get('vertbotServer.mediaPort');
    this.apiPort = configLoader.get('vertbotServer.apiPort');
    this.secret = configLoader.get('vertbotServer.secret');
    this.storageDirectory = configLoader.get('tempFolder');
    this.logger = logger;

    this.server = new https.createServer({
      cert: configLoader.get('vertbotServer.certfile'),
      key: configLoader.get('vertbotServer.keyfile')
    });
    this.server.listen(configLoader.get('vertbotServer.apiPort'));
    this.socket = new WebSocket.Server({
      server: this.server,
      verifyClient: info => {
        return info.req.headers.secret == this.secret
      }
    });

    const app = express();
    app.listen(this.mediaPort);
    app.use(express.static(this.storageDirectory));

    // Cleanup disconnected sockets
    setInterval(() => {
      this.socket.clients.forEach(ws => {
        if (!ws.isAlive) ws.terminate();
        ws.isAlive = false;
        ws.ping(() => null);
      })
    }, 5000);

    // Cleanup tempfiles
    const lifespan = configLoader.get('tempfileLifespan');
    setInterval(() => {
      this.logger.info("Cleaning up tempfiles");
      fs.readdir(this.storageDirectory, (err, files) => {
        if (err) return this.logger.error(err);
        for (let file of files) {
          let filepath = path.join(this.storageDirectory, file);
          fs.stat(filepath, (err, stat) => {
            if (err) return this.logger.error(err);
            if (Date.now() - stat.birthtime > lifespan) {
              fs.unlink(filepath, err => {
                if (err) return logger.error(err);
                this.logger.info("Cleaned up file", filepath);
              });
            }
          });
        }
      });
    }, 60 * 60 * 1000);

    this.socket.on('connection', (ws, req) => {
      logger.info('New websocket connection', req.connection.remoteAddress);

      ws.isAlive = true;
      ws.on('pong', () => ws.isAlive = true);

      ws.on('message', (message) => {
        let packet = JSON.parse(message);
        switch(packet.action) {
          case 'cancel':
            this.cancelByUUID(packet.uuid);
            break;
        }
      });
    });

    /**
     * @typedef {Object} PipelineStorageCoupler
     * @param {PipelineJob} pipelineJob
     * @param {function} cancelCallback
     * @param {FileStorageProvider} storageProvider
     */

    /** @member {Map.<String, PipelineJobStorageCoupler>} */
    this.storageProviders = new Map();
  }

  /**
   * Returns the live connection count
   * @returns {number}
   */
  getConnectionCount() {
    return this.socket.clients.size;
  }

  /**
   * Cancels a job by UUID;
   */
  cancelByUUID(uuid) {
    if (!this.storageProviders.has(uuid)) return;
    this.storageProviders.get(uuid).cancelCallback();
    this.storageProviders.delete(uuid);
  }

  /**
   * Creates a vertbot song and plays it once the FileStorageProvider is closed.
   * @param {PipelineJob} pipelineJob
   * @param {function} cancelCallback called if the downstream client requests
   *   for the pipeline to terminate.
   * @returns {FileStorageProvider}
   */
  async createVertbotSong(pipelineJob, cancelCallback) {
    let provider = new FileStorageProvider(this.storageDirectory, 'wav');
    this.storageProviders.set(provider.uuid, {
      storageProvider: provider,
      cancelCallback,
      pipelineJob
    });
    await this.broadcastPacket({
      uuid: provider.uuid,
      action: 'notifyProcessing',
      pipeline: pipelineJob.pipeline,
      channel: pipelineJob.channel,
      guild: pipelineJob.guild,
      user: pipelineJob.user
    });
    provider.onClose(() => {
      this.storageProviders.delete(provider.uuid);
      this.broadcastPacket({
        uuid: provider.uuid,
        action: 'playSong',
        pipeline: pipelineJob.pipeline,
        channel: pipelineJob.channel,
        guild: pipelineJob.guild,
        user: pipelineJob.user
      });
    });
    return provider;
  }

  /**
   * @typedef {Object} VertPacket
   * @param {string} uuid - unique pipeline result id
   * @param {string} pipeline - the pipeline command
   * @param {string} action - heartbeat, notifyProcessing, playSong
   * @param {string} user - user id
   * @param {string} channel - text channel id
   */

   /**
    * @param {Packet} VertPacket
    */
  async broadcastPacket(packet) {
    this.socket.clients.forEach(ws => {
      ws.send(JSON.stringify(packet));
    })
  }
}

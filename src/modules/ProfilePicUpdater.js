const path = require('path');
module.exports = class ProfilePicUpdater {
  /**
   * @param {Bot} bot
   * @param {Logger} logger
   * @param {string} avatarFolder path to folder with avatars
   */
  constructor(bot, logger, avatarFolder) {
    this.bot = bot;
    this.logger = logger;
    this.avatarFolder = avatarFolder;
  }

  /**
   * Updates the bot's profile picture to the current time
   */
  async updateProfilePic() {
    let hours = new Date().getHours();
    let minutes = new Date().getMinutes();
    let frame = (hours * 6) + Math.floor(minutes/10);
    let imagepath = path.join(this.avatarFolder, `frame-${frame}.png`);
    await this.bot.getClient().user.setAvatar(imagepath);
  }

  /**
   * Starts an interval that updates the bot's pfp every 10 minutes.
   * To stop, clearInterval() the returned timeout.
   * @returns {Timeout}
   */
  startUpdating() {
    return setInterval(() => {
      this.updateProfilePic().catch(ex => {
        this.logger.error('Failed to update pfp', ex);
      });
    }, 10 * 60 * 1000)
  }
}

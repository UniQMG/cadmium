const StraightThrough = require('../../streams/straightThrough');
const { spawn } = require('child_process');

function youtube(sub, callback, getObject, actions) {
  const ytregex = /(?:youtu\.be\/|youtube\.com\/watch\?v=)?([A-Za-z_\d-]{11})/;
  let ytID, ytdl;

  const stream = new StraightThrough();
  function setYoutubeURL(url) {
      ytID = (ytregex.exec(url) || [])[1];

      if (!ytID) throw new Error(
        "Invalid youtube url. Specify video ID or youtube.com/youtu.be url. " +
        "Protocol and subdomain not required."
      );

      getObject(o => o.setStatus(null));
  }
  function createYTDL() {
    ytdl = spawn('youtube-dl', [
      'https://youtube.com/?v=' + ytID, // url selection
      '--restrict-filenames', // STFU errors about assuming this flag
      '-f', 'bestaudio[ext=m4a]', // Get best audio format availible
      '-o', '-', // Write file to stdout
      '-q' // Quiet mode, only print errors
    ]);

    let stderr = [];
    ytdl.stderr.on('data', t => {
      stderr.push(t.toString().trim());
    });
    ytdl.on('exit', code => {
      if (code !== 0)
        getObject().terminate('\n' + stderr.map(e=>'    '+e).join('\n'));
      if (code === 0)
        getObject().finish();
    });

    ytdl.stdout.pipe(stream);
  }

  if (sub.trim().length > 0) {
    try {
      setYoutubeURL(sub.trim());
      createYTDL();
    } catch(ex) {
      callback(ex);
      return;
    }
  } else {
    getObject(o => o.setStatus('waiting for url'));
  }

  callback({
    getInputTypes: () => 'text',
    addInputStream: (stream, id) => {
      if (id >= 1) return false;
      let data = [];
      stream.on('data', d => data.push(d));
      stream.on('end', () => {
        try {
          setYoutubeURL(data.join(''));
          createYTDL();
        } catch(ex) {
          getObject(o => o.terminate(ex));
        }
      });
    },
    getOutputType: () => 'audio:m4a',
    getOutputStream: () => stream,
    destroy: () => {
      ytdl && ytdl.kill()
    }
  });
}

module.exports = youtube;

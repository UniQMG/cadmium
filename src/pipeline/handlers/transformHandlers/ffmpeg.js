const { spawn } = require('child_process');

/**
 * Spawns an instance of ffmpeg
 * @return {
 *   ffmpeg: child_process,
 *   destroy: function,
 *   stream: output,
 * }
 */
function ffmpeg({ inputFlags, outputFlags, onexit, onerror, useSoX }) {
  if (useSoX) {
    var ffmpeg = spawn('sox', [
      ...(inputFlags ? inputFlags.split(' ') : []),
      '-',
      ...(outputFlags ? outputFlags.split(' ') : []),
      '-'
    ]);
  } else {
    var ffmpeg = spawn('ffmpeg', [
      ...(inputFlags ? inputFlags.split(' ') : []),
      '-loglevel', 'warning',
      '-i', 'pipe:0',
      ...(outputFlags ? outputFlags.split(' ') : []),
      '-'
    ]);
  }

  let stderr = [];
  ffmpeg.stderr.on('data', t => {
    stderr.push(t.toString().trim());
  });

  ffmpeg.on('exit', (code, signal) => {
    if (code === 0 && onexit)
      onexit();
    if (code !== 0 && signal === null && onerror)
      onerror(stderr.join('\n'));
    if (signal !== null && onerror)
      onerror("killed by signal: " + signal);
  });

  return {
    ffmpeg,
    streamin: ffmpeg.stdin,
    streamout: ffmpeg.stdout,
    destroy: () => ffmpeg.kill()
  };
}

module.exports = ffmpeg;

const Router = require('instacord2/instacord/router');
const { spawn } = require('child_process')
const { Readable } = require('stream');
const request = require('request');
const app = new Router();
const fs = require('fs');
module.exports = app;

app.cmd('write', (sub, callback, getObject, actions) => {
  const stream = new Readable({
    objectMode: true,
    read() {}
  });

  callback({
    getInputTypes: () => false,
    getOutputType: () => 'text',
    addInputStream: (stream, id) => false,
    getOutputStream: () => {
      setInterval(() => {
        stream.push('packet\n');
      }, 1000);
      return stream
    }
  });
});

app.cmd('livestream', (sub, callback, getObject, actions) => {
  getObject(obj => obj.on('newroot', () => obj.finish()));

  actions.sendData({
    action: 'devCreateLAS'
  });

  callback({
    getInputTypes: () => ['text'],
    addInputStream: async (stream) => {
      stream.on('data', () => {});
    },
    getOutputStream: () => false,
    getOutputType: () => false,
    destroy: () => null
  });
});

const Router = require('instacord2/instacord/router');
const PCMVolume = require('pcm-volume');
const app = new Router();
const fs = require('fs');
module.exports = app;

function ffmpeg({ inputFlags, outputFlags, accepts, format, callback, getObject, useSoX }) {
  let ff = require('./transformHandlers/ffmpeg')({
      onexit: () => getObject().finish('finished successfully'),
      onerror: (err) => getObject().terminate(err),
      outputFlags,
      inputFlags,
      useSoX
  });

  ff.streamin.on('error', (err) => {
    getObject().terminate('Stdin encountered ' + err);
  });

  callback({
    getInputTypes: () => accepts || 'audio',
    getOutputType: () => format,
    addInputStream: (stream, id) => {
      if (stream.id >= 1) return false;
      stream.pipe(ff.streamin);
    },
    getOutputStream: () => ff.streamout,
    destroy: ff.destroy
  });
}


/*
app.cmd('audio-to-pcm', (sub, callback, getObject, msg, actions) => {
  ffmpeg('-f s16le -acodec pcm_s16le', 'pcm:s16le', callback, getObject);
});

app.cmd('pcm-to-wav', (sub, callback, getObject, msg, actions) => {
  let args = new String('-f wav');
  args.inputFlags = '-f s16le -ar 44.1k -ac 2';

  ffmpeg(args, 'audio:wav', callback, getObject, {
    inputTypes: 'pcm:s16le'
  });
});

app.cmd('volume', async (sub, callback, getObject, msg, actions) => {
  let volume = Number.parseFloat(sub) || 100;
  if (sub.indexOf('%') == -1 && volume <= 1)
    volume *= 100; // Assume fraction instead of percent
  let volumeStream = new PCMVolume();
  volumeStream.setVolume(volume/100);

  callback({
    getInputTypes: () => 'pcm:s16le',
    getOutputType: () => 'pcm:s16le',
    addInputStream: (stream, id) => {
      if (stream.id >= 1) return false;
      stream.pipe(volumeStream);
    },
    getOutputStream: () => volumeStream,
    setVolume: (vol) => volumeStream.setVolume(volume/100)
  });
});

*/

app.cmd('resample', (sub, callback, getObject, msg, actions) => {
  ffmpeg({
    inputFlags: '-t wav',
    outputFlags: '-t wav -r 44.1k',
    accepts: 'audio:wav',
    format: 'audio:wav',
    useSoX: true,
    getObject,
    callback
  });
});

app.cmd('audio-to-mp3', (sub, callback, getObject, msg, actions) => {
  ffmpeg({
    outputFlags: '-f mp3',
    format: 'audio:mp3',
    getObject,
    callback
  });
});

app.cmd('audio-to-wav', (sub, callback, getObject, msg, actions) => {
  ffmpeg({
    outputFlags: '-f wav',
    format: 'audio:wav',
    getObject,
    callback
  });
});

app.cmd('audio-to-flac', (sub, callback, getObject, msg, actions) => {
  ffmpeg({
    outputFlags: '-f flac',
    format: 'audio:flac',
    getObject,
    callback
  });
});

app.cmd('nightcore', (sub, callback, getObject, msg, actions) => {
  let sampleRate = Math.floor((Number.parseFloat(sub) || 120)/100 * 44100);
  ffmpeg({
    outputFlags: '-filter:a asetrate=' + sampleRate + ' -f wav',
    format: 'audio:wav',
    getObject,
    callback
  });
});

app.cmd('speed', (sub, callback, getObject, msg, actions) => {
  let newTempo = Math.floor(Number.parseFloat(sub) || 120)/100;
  if (newTempo < 0.5 || newTempo > 2.0) {
    callback(new Error(
      "ffmpeg only accepts speeds of 50% to 200%. If you need a wider range, " +
      "you can chain multiple speed calls."
    ));
    return;
  }
  ffmpeg({
    outputFlags: `-filter:a atempo=${newTempo} -f wav`,
    format: 'audio:wav',
    getObject,
    callback
  })
});

app.cmd('phase', (sub, callback, getObject, msg, actions) => {
  ffmpeg({
    outputFlags: '-filter:a aphaser -f wav',
    format: 'audio:wav',
    getObject,
    callback
  });
});

app.cmd('compressor', (sub, callback, getObject, msg, actions) => {
  let inputs = sub.trim().split(' ');
  let filters = [
    "threshold=" + (parseFloat(inputs[0]) || 0.125),
    "ratio=" + (parseFloat(inputs[1]) || 2),
    "attack=" + (parseFloat(inputs[2]) || 20),
    "release=" + (parseFloat(inputs[3]) || 250)
  ];
  ffmpeg({
    outputFlags: `-af acompressor=${filters.join(':')} -f wav`,
    format: 'audio:wav',
    getObject,
    callback
  });
});

const eqLevels = [31.5, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000];
app.cmd('equalizer', (sub, callback, getObject, msg, actions) => {
  let gains = sub.split(/(,\s+|\s+)/)
                 .filter(e=>e.trim().length)
                 .map(e => Number.parseFloat(e));

  if (gains.length !== 10)
    return callback(new Error(`Please use exactly 10 EQ gain settings. (You used ${gains.length})`));

  let filters = eqLevels.map((freq, i) => {
    return `entry(${freq},${gains[i]})`
  }).join(';');

  ffmpeg({
    outputFlags: `-af firequalizer=gain_entry='${filters}' -f wav`,
    format: 'audio:wav',
    getObject,
    callback
  })
});

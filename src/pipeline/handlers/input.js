const Router = require('instacord2/instacord/router');
const { spawn } = require('child_process')
const { Readable } = require('stream');
const request = require('request');
const app = new Router();
const fs = require('fs');
module.exports = app;

app.cmd('echo', (sub, callback, getObject) => {
  const stream = new Readable({
    objectMode: true,
    read() {}
  });

  callback({
    getInputTypes: () => false,
    getOutputType: () => 'text',
    addInputStream: (stream, id) => false,
    getOutputStream: () => {
      setTimeout(() => {
        stream.push(sub.trim() + '\n');
        stream.destroy();
        getObject(o => o.finish());
      })
      return stream
    }
  });
});
app.cmd('ytsearch', (sub, callback, getObject, actions) => {
  let ytdl = spawn('youtube-dl', [
    'ytsearch:"' + sub.replace(/\"/g, '') + '"', // search for video
    '--restrict-filenames', // STFU errors about assuming this flag
    '--get-id', // print output
    '-o', '-', // write file to stdout
    '-q' // quiet mode, only print errors
  ]);

  var failed = false;
  var stderr = [];
  ytdl.stderr.on('data', err => {
    err = err.toString().trim();
    stderr.push(err);
    if (failed || !(/^[A-Za-z_\d-]{11}$/.test(err))) {
      getObject().terminate(stderr.join(''));
      failed = true;
    }
  });
  ytdl.on('exit', err => {
    getObject().finish();
  });

  callback({
    getInputTypes: () => false,
    getOutputType: () => 'text',
    addInputStream: (stream, id) => false,
    getOutputStream: () => ytdl.stderr
  });
});
app.cmd('youtube', (sub, callback, getObject, actions) => {
  require('./inputHandlers/youtube')(sub, callback, getObject, actions);
});
app.cmd('url', (sub, callback, getObject, actions) => {
  new Promise((resolve, reject) => {
    request({ method: 'HEAD', url: sub.trim() }, (err, res, body) => {
      if (err) {
        callback(err);
        return;
      }
      console.log(res.statusCode, sub.trim());
      if (res.statusCode !== 200) {
        callback(new Error(`GET \`${sub.trim()}\`: ${res.statusCode} ${res.statusMessage}`));
        return;
      }

      let contentType = res.headers['content-type'];
      console.log("Content type '" + contentType + "'")
      let contentRegex = /^(.+)\/([^;]+)(;.+)?/.exec(contentType);
      if (!contentRegex) {
        callback(new Error("Unsupported content type: " + contentType))
        return;
      }
      let [match, t1, t2] = contentRegex;

      switch (t1) {
        case 'audio':
          switch(t2) {
            case   'acc': return resolve('audio:aac');
            case   'ogg': return resolve('audio:ogg');
            case   'wav': return resolve('audio:wav');
            case 'x-wav': return resolve('audio:wav');
            case   'mp3': return resolve('audio:mp3');
            case   'mp4': return resolve('audio:mp4');
            case  'mpeg': return resolve('audio:mp3');
            default:
              callback(new Error("Unsupported content type: " + contentType));
          }
          break;

        default:
          callback(new Error("Unsupported content type: " + contentType));
      }
    });
  }).then(contentClass => {
    callback({
      getInputTypes: () => false,
      addInputStream: (stream, id) => false,
      getOutputType: () => contentClass,
      getOutputStream: () => {
        return request(sub.trim()).on('end', () => getObject().finish());
      }
    });
  });
});

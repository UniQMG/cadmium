const Router = require('instacord2/instacord/router');
const { spawn } = require('child_process');
const split2 = require('split2');
const app = new Router();
const fs = require('fs');
module.exports = app;

function safePipe(stream, getObject, actions, action, key, extraData) {
  let terminated = false;
  getObject(obj => obj.on('newroot', () => obj.finish()));

  stream.on('data', chunk => {
    if (terminated) return;
    let [noBP, outStream] = actions.sendData(Object.assign({
      action: action,
      [key]: chunk
    }, extraData || {}));
    if (!noBP) {
      stream.pause();
      outStream.once('drain', () => {
        stream.resume();
      });
    }
  });

  stream.on('close', () => {
    getObject(o => o.finish());
  });

  return {
    terminate: function terminate() {
      terminated = true;
      getObject(o => o.finish());
    }
  }
}

app.cmd('reply', (sub, callback, getObject, actions) => {
  var stream;

  getObject(obj => obj.on('newroot', () => obj.finish()));
  callback({
    getInputTypes: () => ['text'],
    addInputStream: async (stream) => {
      stream = safePipe(stream.pipe(split2()), getObject, actions, 'reply', 'text');
    },
    getOutputStream: () => false,
    getOutputType: () => false,
    destroy: () => stream && stream.terminate()
  });
});

app.cmd('voicechat', async (sub, callback, getObject, actions) => {
  var stream;

  getObject(obj => obj.on('newroot', () => obj.finish()));
  callback({
    getInputTypes: () => 'audio:wav',
    addInputStream: (stream, id) => {
      stream = safePipe(stream, getObject, actions, 'voicechat', 'data');
    },
    getOutputStream: false,
    getOutputType: false,
    destroy: () => stream && stream.terminate()
  })
});

function makeAudioUpload(name, format) {
  app.cmd(name, async (sub, callback, getObject, actions) => {
    var stream;

    getObject(obj => obj.on('newroot', () => obj.finish()));
    callback({
      getInputTypes: () => format,
      addInputStream: (stream, id) => {
        stream = safePipe(stream, getObject, actions, 'upload', 'data', {
          format: name.split('-')[1]
        });
      },
      getOutputStream: false,
      getOutputType: false,
      destroy: () => stream && stream.terminate()
    });
  });
}

makeAudioUpload('audioupload-wav', 'audio:wav');
makeAudioUpload('audioupload-mp3', 'audio:mp3');
makeAudioUpload('audioupload-m4a', 'audio:m4a');
makeAudioUpload('audioupload-flac', 'audio:flac');
app.cmd('audioupload', (sub, callback, getObject, actions) => {
  callback(new Error("Please specify a format (audioupload-wav, audioupload-mp3, audioupload-m4a, audioupload-flac)"));
});

let numsStreams = 0;
let seed = Math.floor(Math.random() * 1e6);
app.cmd('vertbot', async (sub, callback, getObject, actions) => {
  const format = 'wav';
  var key = Date.now() + '-' + seed + '-' + (numsStreams++) + '.' + format;
  var stream;

  actions.sendData({
    action: 'vertbotNew',
    streamid: key
  });

  getObject(obj => obj.on('newroot', () => obj.finish()));
  callback({
    getInputTypes: () => 'audio:' + format,
    addInputStream: (stream, id) => {
      stream = safePipe(stream, getObject, actions, 'vertbot', 'data', {
        streamid: key
      });
    },
    getOutputStream: false,
    getOutputType: false,
    destroy: () => stream && stream.terminate()
  });
});

const { Transform } = require('stream');

module.exports = class StraightThrough extends Transform {
  constructor(options) {
    super(options);
  }

  _transform(chunk, encoding, callback) {
    callback(null, chunk);
  }

  _flush(callback) {
    callback(null);
  }

  _final(callback) {
    callback(null);
  }
}

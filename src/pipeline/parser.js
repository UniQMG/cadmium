const PipelineObject = require('./pipelineObject');
const path = require('path');

const handlers = {
  input: require('./handlers/input.js'),
  output: require('./handlers/output.js'),
  transform: require('./handlers/transform.js'),
  dev: require('./handlers/dev.js'),
}

module.exports = createPipeline;
module.exports.handlers = Object.freeze(handlers);

class Pipeline extends Array {
  constructor() {
    super();
    this.objectsFinished = 0;
    this.onFinishCallbacks = [];
  }

  push(obj) {
    if (obj.finished) {
      this.objectsFinished++;
    } else {
      obj.once('finish', () => {
        this.objectsFinished++;
        if (this.objectsFinished === this.length)
          this.onFinishCallbacks.forEach(cb => cb())
      });
    }

    super.push(obj);
  }

  status() {
    return this.map((obj, id) => {
      return "#" + id + ": " + obj.getStatus();
    }).join('\n')
  }

  terminate(reason) {
    this.forEach(obj => {
      obj.terminate(reason);
    });
  }

  onEnd(callback) {
    if (this.length == this.finished) {
      callback();
      return;
    }
    this.onFinishCallbacks.push(callback);
  }
}

/**
 * Creates a new pipeline
 * @param {String} pipeline: The pipeline command
 * @param {...Object} args: Arguments to pass to the handler applets
 *   (e.g. discord message to reply to)'
 * @return Promise<PipelineObject>: returns the constructed pipeline, or rejects
 *   on invalid pipeline syntax or semantics.
 */
async function createPipeline(pipeline, ...args) {
  pipeline = pipeline.trim().replace('\n', ' ');
  const objects = new Pipeline();

  let nextMode = '';

  try {
    const matcher = /(.+?)(&&|\||;|$)/ym;
    while (match = matcher.exec(pipeline)) {
      let [fullmatch, command, seperator] = match;
      command = command.trim();

      const arguments = command.split(/\s+/);
      const operationName = arguments[0];
      const operationHandler = handlers[operationName];

      if (!operationHandler) {
        throw new Error(`Operation ${operationName} not found.`);
        continue;
      }

      let error;
      let pipeObject;
      let handlerArguments = arguments.slice(1).join(' ');

      function onError(router, err) {
        error = err;
        err.handled = true;
      }

      const getObjectCallbacks = [];
      operationHandler.on('resolutionException', onError);

      let pipelineResolve, pipelineReject;
      let getPipelineProcessor = new Promise((resolve, reject) => {
        pipelineResolve = resolve;
        pipelineReject = reject;
      });

      // sub, callback, getObject, msg, actions
      let sub = handlerArguments;
      function callback(_proc) {
        if (_proc instanceof Error) {
          pipelineReject(_proc);
          return;
        }
        pipelineResolve(_proc);
      }
      function getObject(callback) {
        if (callback && pipeObject)
          callback(pipeObject)
        if (callback && !pipeObject)
          getObjectCallbacks.push(callback);
        return pipeObject;
      }
      let msg = args[0];
      let actions = args[1];

      try {
        await operationHandler.resolve(sub, callback, getObject, msg, actions)
      } catch(ex) {
        if (ex) throw ex; // If an actual error occurred, throw it
        if (error) throw error; // Throw resolutionException error, if fired.
        // Otherwise throw a not found error
        throw new Error(
          `Subcommand '${handlerArguments}' on '${arguments[0]}' does not exist`
        );
      }
      operationHandler.removeListener('resolutionException', onError);

      let processor = await getPipelineProcessor;
      if (error) {
        throw new Error(
          `Invarient violated: Error ocurred in resolution after resolution.` +
          `Please report to bot author with full stack trace (shown below).` +
          error.stack
        );
      }

      if (!processor) {
        throw new Error(
          `Failed to create pipeline object ${command}, unknown reason.`
        );
      }

      pipeObject = new PipelineObject(command, processor);
      getObjectCallbacks.forEach(cb => cb(pipeObject));

      switch (nextMode) {
        case '&&': throw new Error(`&& not supported yet`);
        case '|':
          objects[objects.length-1].chain(pipeObject);
          break;
        case ';': break;
        case '': break;
      }
      nextMode = seperator;

      objects.push(pipeObject);
    }

  } catch(ex) {
    objects.forEach(object => object.terminate(ex));
    throw ex; // Handled by calling code as a rejection
  }
  return objects;
}

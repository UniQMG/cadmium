const { EventEmitter } = require('events');

/**
 * A PipelineObject is a lifecycle manager for pipeline commands that
 * handles IO and enforces strict type checking between pipeline segments.
 * @emits read-data<Object streamData, Number length>: when data is consumed from a parent by this object
 * @emits finish: when this object has finished processing, also emitted after error.
 * @emits write-data<Number length>: when data is produced by this object
 * @emits terminated<Error reason>: when this object dies due to an error
 * @emits newroot: when all this object's parents die
 */
module.exports = class PipelineObject extends EventEmitter {
  /**
   * Creates a new PipelineObject
   * @param {String} name: A name for this pipeline object.
   * @param {TODO: create type} processor: the processor which reads inputs and
   *   writes the outputs associated with this pipeline object.
   */
  constructor(name, processor) {
    super();
    this.name = name;
    this.error = null;
    this.errors = [];
    this.errored = false;
    this.finished = false;
    this.statusText = null;
    this.masters = []; // objects piping to this one
    this.terminatedMasters = 0; // finshed master object reference countering
    this.inputStreams = []; // arbitrary readable streams
    this.childrenObjects = []; // chained objects
    this.totalDataProcessed = 0;
    this.inboundDataProcessed = 0;
    this.outboundDataProcessed = 0;
    this.lastInboundPacket = Date.now();
    this.lastOutboundPacket = Date.now();
    this.processor = processor;
    this.created = Date.now();

    //console.log("Created pipeline object", name);

    this.on('read-data', (stream, length) => {
      //console.log(`${this.name}: Read ${length} bytes from input stream #${stream.id} (total: ${stream.read})`);
      this.totalDataProcessed += length;
      this.inboundDataProcessed += length;
      this.lastInboundPacket = Date.now();
    });

    this.on('write-data', (length) => {
      this.totalDataProcessed += length;
      this.outboundDataProcessed += length;
      this.lastOutboundPacket = Date.now();
    });

    if (processor.getOutputStream)
      this.outputStream = processor.getOutputStream();

    if (this.outputStream) {
      // Reduce high water marks to reduce streaming delays associated
      // with a mid-execution pipeline change during live playback
      if (this.outputStream._writableState)
        this.outputStream._writableState.highWaterMark = 1024;

      if (this.outputStream._readableState)
        this.outputStream._readableState.highWaterMark = 1024;

      this.outputStream.on('data', (data) => {
        this.emit('write-data', data.length)
      });
      this.outputStream.on('error', (err) => {
        this.terminate(err);
      });
    }
  }

  /**
   * Performs any neccessary cleanup of the object, and attempts to
   * immediately stop all streams and processes. Sets the 'errored' flag.
   * Does nothing if the pipeline has already terminated.
   * @param {String} reason: why the pipeline was terminated
   */
  terminate(reason) {
    if (this.errors.indexOf(reason) === -1) {
      if (reason.toString().trim().length !== 0)
        this.errors.push(reason);
      this.error = new Error(
        'Pipeline object terminated.' +
        this.errors.map(e => '\n\t' + e).join('')
      );
    }

    if (!this.errored) {
      this.errored = true;
      this.emit('terminated', this.error);
      this.finish();
    }
  }

  /**
   * Marks the pipeline as finished.
   */
  finish() {
    if (!this.finished) {
      this.finished = 1;
      this.emit('finish');

      if (this.processor.destroy)
        this.processor.destroy();
      this.childrenObjects.forEach(child => {
        child._superProcessFinish();
      });
    }
  }

  /**
   * Adds a master object that should call _superProcessFinish() on this
   * object once it finishes.
   * @param {PipelineObject} master
   */
  _addMaster(master) {
    this.masters.push(master);
  }

  /**
   * Called by an object further up the chain when it finishes.
   * @emit newroot: when all parent objects have finished.
   */
  _superProcessFinish() {
    this.terminatedMasters++;
    if (this.terminatedMasters == this.masters.length)
      this.emit('newroot');
  }

  /**
   * Updates the status for the pipelineObject
   * (e.g. running, waiting for link, etc.)
   * @param {String} text: the new status
   */
  setStatus(text) {
    this.statusText = text;
  }

  /**
   * Returns the status of the object as a human-readable string
   * @return {String}
   */
  getStatus() {
    let status = '';
    let state = this.errored
      ? 'errored'
      : this.finished
        ? 'finished'
        : this.atHighWaterMark()
          ? 'running [bottlenecked]'
          : 'running';
    if (this.statusText)
      state = this.statusText + ' (' + state + ')';
    status += '"' + this.name + '". Status: ' + state + '.\n';
    if (this.error) {
      status += 'Error: '
      status += this.error.message + '\n';
    }
    status += 'Buffers: ' + this.inputBuffered() + ' bytes inbound, ' + this.outputBuffered() + ' bytes outbound.\n';
    status += 'Total data processed: ' + this.inboundDataProcessed + ' bytes in, ' + this.outboundDataProcessed + ' bytes out.\n';

    //let bpsin = Math.floor(this.inboundDataProcessed / (Date.now() - this.created));
    //let bpsout = Math.floor(this.outboundDataProcessed / (Date.now() - this.created));
    //status += 'Lifetime bitrate: ' + bpsin + ' kbps in, ' + bpsout + ' kbps out.\n';
    this.childrenObjects.forEach(child => {
      status += `Connected to '${child.name}'.\n`;
    });
    return status;
  }

  /**
   * Checks if this object is compatible with another, i.e. it shares the same
   * classes with the target object. This is a forward-operation, i.e.
   * this.chain(target) is valid if this returns true, but not neccessarily
   * target.chain(this).
   *
   * e.g.
   * [foo, bar] is compatible with [foo], [bar], [foo, bar] but not [baz]
   * [foo] is compatible with [foo] but not [bar]
   * [foo:sub] is compatible with [foo:sub], [foo] but not [foo:bar], [foo:baz]
   *
   * @param {PipelineObject} object: the object to check compatibility with.
   */
  compatibleWith(object) {
    if (!object.supportsInput())
      throw new Error(`Pipeline Object "${object.name}" does not support input.`);
    if (!this.supportsOutput())
      throw new Error(`Pipeline Object "${this.name}" does not support output.`);

    let acceptTypes = object.processor.getInputTypes();
    let type = this.processor.getOutputType();
    if (typeof acceptTypes === 'string' || acceptTypes instanceof String)
      acceptTypes = [acceptTypes];

    loopTypes: for (var atype of acceptTypes) {
      if (atype === '*') return true;
      let acceptClasses = atype.split(':');
      let classes = type.split(':');

      // if the accepted class length extends the proferred class length,
      // it's impossible for them to be compatible ('foo' cannot be 'foo:bar')
      // (In other words, the type is not specific enough)
      if (acceptClasses.length > classes.length)
        continue loopTypes;

      // Iterate accepted classes
      for (var i = 0; i < acceptClasses.length; i++)
        if (acceptClasses[i] != classes[i]) // If a class mismatches, skip
          continue loopTypes;

      // All classes match, the types are compatible
      return true;
    }

    return false;
  }

  /**
   * Checks if this object supports inputs, and returns their types if so
   * @return {String[]|false}
   */
  supportsInput() {
    if (!this.processor.getInputTypes) return false;
    let types = this.processor.getInputTypes();
    if (typeof types === 'string' || types instanceof String)
      return [types];
    return types;
  }

  /**
   * Checks if this object supports outputs, and returns the type if so
   * @return {String|false}
   */
  supportsOutput() {
    return this.processor.getOutputType && this.processor.getOutputType();
  }

  /**
   * Returns the amount of buffered data waiting to be processed by this object
   * @return {Number}
   */
  inputBuffered() {
    return this.inputStreams.reduce((amt, streamdata) => {
      if (!streamdata.stream._readableState) return;
      return amt + streamdata.stream._readableState.length;
    }, 0);
  }

  /**
   * Returns the amount of data waiting to be processed by the next stage.
   * @return {Number}
   */
  outputBuffered() {
    if (!this.outputStream) return 0;
    if (!this.outputStream._readableState) return "?";
    return this.outputStream._readableState.length;
  }

  /**
   * @return {Boolean} if the output stream of this object is operating at it's
   *   high-water mark and is behind a limiting element in the pipeline.
   */
  atHighWaterMark() {
    if (!this.outputStream) return false;
    if (!this.outputStream._readableState) return false;
    return this.outputBuffered() > this.outputStream._readableState.highWaterMark;
  }

  /**
   * Chains this object to another, connecting this object's processor's output
   * with the target object's input.
   * @param {PipelineObject} object
   */
  chain(object) {
    //console.log(`${this.name}: Chain to ${object.name}.`);
    let outStream = this.processor.getOutputStream();
    if (outStream === false) {
      throw new Error(
        `Pipeline object '${this.name}' cannot pipe to another pipeline object.`
      );
    }
    if (!this.compatibleWith(object)) {
      throw new Error(
        `Pipeline object '${object.name}' cannot accept input from ${this.name}`
        + ` (Got '${this.supportsOutput()}', expected one of `
        + `[${object.supportsInput().map(e=>`'${e}'`).join(', ')}]).`
      );
    }
    object.pipeFrom(outStream);
    this.childrenObjects.push(object);
    object._addMaster(this);
  }


  /**
   * Adds a stream input to this object
   * @param {ReadableStream} readableStream
   */
  pipeFrom(readableStream) {
    let stream = {
      stream: readableStream,
      opened: Date.now(),
      read: 0,
      id: this.inputStreams.length
    };
    this.inputStreams.push(stream);

    readableStream.on('data', (val) => {
      stream.read += val.length;
      this.emit('read-data', stream, val.length);
    });

    let result = this.processor.addInputStream(readableStream, stream.id);
    if (result === false) {
      throw new Error(
        `Pipeline object '${this.name}' does not accept input or has reached ` +
        `it's input limit.`
      );
    }
    //console.log(`${this.name}: Added input stream #${stream.id}`);
  }
}

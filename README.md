note: This bot is open-source for code sample purposes, but it is not licensed. I'll probably get around to it, eventually.

See https://uniq.mg/cadmium

Cadmium is an audio processing bot. It uses nodes connected together using pipes to provide fine control over how data is moved and transformed, similar to cli tools such as bash.

Cadmium is not a playback bot, and while it can send audio to voice chat, it does not offer usual music bot features such as playlists, volume control, or pausing. Features integration with Vertbot, so you can export audio and play it using Vertbot's proper music bot abilities.

An example pipeline, which would fetch audio from a youtube video, nightcore it, then upload it as an mp3 file:
`input youtubed Qw4w9WgXcQ | transform nightcore 150% | transform audio-to-mp3 | output audioupload-mp3`
Advanced usage and documentation available via the 'man' and 'help' commands.
Default prefix: `caddy` or a mention.

